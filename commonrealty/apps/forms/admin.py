from django.contrib import admin

# Register your models here.
from commonrealty.apps.forms.models import GeneralContactForm, ProjectContactForm,\
    AgentApplicationForm, AgentSubClientApplicationForm

admin.site.register(GeneralContactForm)
admin.site.register(ProjectContactForm)
admin.site.register(AgentApplicationForm)
admin.site.register(AgentSubClientApplicationForm)