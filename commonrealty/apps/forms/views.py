import requests
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework_simplejwt import authentication

from commonrealty import settings
from commonrealty.apps.forms.models import GeneralContactForm, ProjectContactForm, AgentApplicationForm
from commonrealty.apps.projects.models import Project
from commonrealty.apps.users.models import UserExt
from commonrealty.apps.users.views import create_user


@api_view(['POST'])
@authentication_classes([authentication.JWTAuthentication])
def post_form(request):
    form_type = request.data.get('type') or None

    if form_type == 'general':
        first_name = request.data.get('firstName') or None
        last_name = request.data.get('lastName') or None
        message = request.data.get('message') or None
        phone = request.data.get('phone') or None
        email = request.data.get('email') or None

        if not first_name or not last_name or not message or not email:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        form = GeneralContactForm(
            first_name=first_name,
            last_name=last_name,
            message=message,
            email=email,
            phone=phone,
            user=User.objects.get(id=request.user.id) if request.user.id else None
        )
        form.save()
        if form.id:
            return Response(status=status.HTTP_201_CREATED)

    if form_type == 'project':
        name = request.data.get('name') or None
        enquiry = request.data.get('enquiry') or None
        phone = request.data.get('phone') or None
        email = request.data.get('email') or None
        whatsapp = request.data.get('whatsapp') or None
        wechat = request.data.get('wechat') or None
        requested = request.data.get('requested') or None
        project_slug = request.data.get('project') or None

        if not phone or not email or not enquiry or not project_slug:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        project = Project.objects.filter(slug=project_slug)
        if not project.exists():
            return Response(status=status.HTTP_400_BAD_REQUEST)

        form = ProjectContactForm(
            name=name,
            email=email,
            phone=phone,
            message=enquiry,
            whatsapp=whatsapp,
            wechat=wechat,
            enquiry_documents=','.join(requested) or None,
            project=project.first(),
            user=User.objects.get(id=request.user.id) if request.user.id else None
        )

        form.save()
        if form.id:
            req = requests.post(settings.SALESFORCE_TOKEN_URL)
            auth_header = '{} {}'.format(req.json().get('token_type'), req.json().get('access_token'))
            data = request.data
            data['id'] = form.id
            data['suburb'] = project.first().suburb.name
            lead_sync_req = requests.post(settings.SALESFORCE_LEAD_SYNC_URL, json=data, headers={"Authorization": auth_header})
            if lead_sync_req.status_code == 200:
                return Response(status=status.HTTP_201_CREATED)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    if form_type == 'agent-application':
        company = request.data.get('company') or None
        role = request.data.get('role') or None
        industry = request.data.get('industry') or None
        phone = request.data.get('phone') or None

        if not company or not role or not industry or not phone:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if not request.user.id:
            name = request.data.get('name') or None
            email = request.data.get('email') or None
            password = request.data.get('password')\
                if request.data.get('password') == request.data.get('password_confirm') else None

            if not name or not email or not password:
                return Response(status=status.HTTP_400_BAD_REQUEST)

            name_parse = name.split(' ')
            first_name = name_parse[0]
            last_name = name_parse[1] or ''
            user_id = create_user(
                email=email,
                phone=phone,
                password=password,
                first_name=first_name,
                last_name=last_name,
                company=company,
                industry=industry,
                role=role
            )
        else:
            user_id = request.user.id

        user = get_object_or_404(User, id=user_id)

        form = AgentApplicationForm(
            name=user.get_full_name(),
            email=user.email,
            company=user.ext.company,
            industry=user.ext.industry,
            phone=user.ext.phone,
            user=user
        )
        form.save()
        if form.id:
            return Response(status=status.HTTP_201_CREATED)

    return Response(status=status.HTTP_400_BAD_REQUEST)
