from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from commonrealty.apps.projects.models import Project
from commonrealty.apps.utils import ModelUtils


class GeneralContactForm(models.Model):
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300)
    email = models.EmailField()
    phone = models.CharField(max_length=20, null=True, blank=True)
    message = models.TextField()
    user = models.ForeignKey(
        User,
        related_name='sent_general_enquiries',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    created = models.DateTimeField(default=timezone.now)
    responded = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '{} - {}'.format(self.first_name + ' ' + self.last_name, self.email)

    def list_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': '{} {}'.format(self.first_name, self.last_name),
            'user': {
                'name': self.user.get_full_name(),
                'id': self.user_id
            } if self.user else None,
            'email': self.email,
            'phone': self.phone,
            'message': self.message,
            'created': self.created,
            'responded': self.responded
        }

    @staticmethod
    def filters_admin():
        return {
            'responded': [
                {
                    'text': 'Responded',
                    'value': 'responded'
                },
                {
                    'text': 'Not Responded',
                    'value': 'not_responded'
                }
            ]
        }


class ProjectContactForm(models.Model):
    name = models.CharField(max_length=300, null=True, blank=True)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    message = models.TextField()
    whatsapp = models.CharField(max_length=300, null=True, blank=True)
    wechat = models.CharField(max_length=300, null=True, blank=True)
    enquiry_documents = models.CharField(max_length=300, null=True, blank=True)
    user = models.ForeignKey(
        User,
        related_name='sent_project_enquiries',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    project = models.ForeignKey(
        Project,
        related_name='enquiries',
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField(default=timezone.now)
    responded = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '{} - {}'.format(self.project.name, self.email)

    def list_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
            'phone': self.phone,
            'message': self.message,
            'whatsapp': self.whatsapp,
            'wechat': self.wechat,
            'project': {
                'name': self.project.name,
                'id': self.project_id
            },
            'user': {
                'name': self.user.get_full_name(),
                'id': self.user_id
            } if self.user else None,
            'enquiry_documents': self.enquiry_documents,
            'created': self.created,
            'responded': self.responded
        }

    @staticmethod
    def filters_admin():
        return {
            'project': map(lambda item: (item, Project.objects.get(id=item).name),
                           ProjectContactForm.objects.values_list('project', flat=True).distinct()),
        }


class AgentApplicationForm(models.Model):
    status_enum = (
        (0, 'Unknown'),
        (1, 'Submitted'),
        (2, 'Approved'),
        (3, 'Rejected'),
        (4, 'Closed'),
    )
    name = models.CharField(max_length=300)
    email = models.EmailField()
    company = models.CharField(max_length=300)
    industry = models.CharField(max_length=300)
    phone = models.CharField(max_length=300)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='agent_applications'
    )
    created = models.DateTimeField(default=timezone.now)
    responded = models.DateTimeField(null=True, blank=True)
    status = models.IntegerField(choices=status_enum, default=1)

    def __str__(self):
        return '{0} - {1}'.format(self.company, self.name)

    def list_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
            'company': self.company,
            'industry': self.industry,
            'phone': self.phone,
            'user': {
                'name': self.user.get_full_name(),
                'id': self.user_id
            },
            'status': {
                'text': ModelUtils.get_string_value_from_enum(
                    self.status_enum, self.status
                ) if self.status else None,
                'color': ModelUtils.map_enum_value(
                    self.status,
                    ['#f50', '#f50', '#87d068', '#cd201f', '#f50']
                ) if self.status else None
            },
            'created': self.created,
            'responded': self.responded
        }

    @staticmethod
    def filters_admin():
        return {
            'status': list(AgentApplicationForm.status_enum)[1:],
            'industry': AgentApplicationForm.objects.values_list('industry', flat=True).distinct(),
        }


class AgentSubClientApplicationForm(models.Model):
    status_enum = (
        (0, 'Unknown'),
        (1, 'Submitted'),
        (2, 'Approved'),
        (3, 'Rejected'),
        (4, 'Closed'),
    )
    agent = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='for_sub_client_applications'
    )
    sub_client_name = models.CharField(max_length=300, null=True, blank=True)
    sub_client_email = models.EmailField(null=True, blank=True)
    sub_client_phone = models.CharField(max_length=300, null=True, blank=True)
    sub_client_user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='by_agent_request_applications',
        null=True,
        blank=True
    )
    created = models.DateTimeField(default=timezone.now)
    responded = models.DateTimeField(null=True, blank=True)
    status = models.IntegerField(choices=status_enum, default=1)

    def __str__(self):
        return '{} -> {}'.format(
            self.sub_client_user.get_full_name() if self.sub_client_user else self.sub_client_name,
            self.agent.get_full_name()
        )
