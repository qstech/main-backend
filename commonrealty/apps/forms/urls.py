from django.urls import path

from commonrealty.apps.forms.views import post_form
from commonrealty.apps.forms.admin_views import get_admin_form_list, post_admin_form_status,\
    external_update_project_form_status

app_name = 'forms'

urlpatterns = [
    path('', post_form, name='post_form'),
    # admin
    path('admin/list/', get_admin_form_list, name='get_admin_form_list'),
    path('admin/post/', post_admin_form_status, name='post_admin_form_status'),
    # exteral
    path('admin/respond/', external_update_project_form_status, name='external_update_project_form_status')
]
