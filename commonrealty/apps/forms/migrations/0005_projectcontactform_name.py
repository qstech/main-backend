# Generated by Django 3.1.3 on 2021-01-11 07:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0004_agentapplicationform'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcontactform',
            name='name',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
    ]
