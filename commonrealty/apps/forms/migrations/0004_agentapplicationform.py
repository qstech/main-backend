# Generated by Django 3.1.3 on 2020-11-19 07:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('forms', '0003_auto_20201119_0659'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgentApplicationForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
                ('email', models.EmailField(max_length=254)),
                ('company', models.CharField(max_length=300)),
                ('industry', models.CharField(max_length=300)),
                ('phone', models.CharField(max_length=300)),
                ('created', models.DateTimeField(default=django.utils.timezone.now)),
                ('responded', models.DateTimeField(blank=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'Unknown'), (1, 'Submitted'), (2, 'Approved'), (3, 'Rejected'), (4, 'Closed')], default=1)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='agent_applications', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
