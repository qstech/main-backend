from datetime import datetime

from django.core.paginator import Paginator
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.generics import get_object_or_404
from rest_framework_simplejwt import authentication

from commonrealty.apps.forms.models import GeneralContactForm, AgentApplicationForm, ProjectContactForm
from commonrealty.apps.users.custom_permission import IsAuthenticatedAndInternal
from commonrealty.apps.users.models import UserExt
from commonrealty.apps.utils import HttpUtils


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_form_list(request):
    form_type = request.GET.get('type') or 'general'
    per_page = request.data.get('pageSize') or 10
    page = request.data.get('current') or 1
    filters = request.data.get('filter') or None
    sorter = request.data.get('sorter') or 'id'
    sorter_direction = request.data.get('sorterDirection') or '-'

    if form_type == 'agent':
        data = AgentApplicationForm.objects.all()
    elif form_type == 'project':
        data = ProjectContactForm.objects.all()
    else:
        data = GeneralContactForm.objects.all()

    if filters:
        filter_options = {}
        for i in filters:
            if request.data.get('filter')[i]:
                filter_options[i + '__in'] = request.data.get('filter')[i]
        data = data.filter(**filter_options)

    if sorter == 'name' and form_type == 'general':
        sorter = 'first_name'

    data = data.order_by('{}{}'.format(sorter_direction, sorter))
    paginator = Paginator(data, per_page)

    response = {
        'total': data.count(),
        'current': page,
        'per_page': per_page or 10,
        'total_pages': paginator.num_pages,
        'items': map(lambda item: item.list_item_serializer_admin(), paginator.get_page(page)),
    }
    if form_type == 'agent':
        response['filters'] = AgentApplicationForm.filters_admin()
    if form_type == 'project':
        response['filters'] = ProjectContactForm.filters_admin()
    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['PUT'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def post_admin_form_status(request):
    form_type = request.GET.get('type') or 'general'
    form_id = request.data.get('form_id')
    value = request.data.get('value')

    if form_type == 'project':
        query_set = ProjectContactForm
    elif form_type == 'agent':
        query_set = AgentApplicationForm
    else:
        query_set = GeneralContactForm
    if not form_id:
        return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST)
    form = query_set.objects.filter(id=form_id)
    if form.exists():
        if form_type == 'agent' and value == 2:
            form.update(responded=datetime.now(), status=2)
            user_ext = get_object_or_404(UserExt, user=form.first().user_id)
            user_ext.user_type = 2
            user_ext.save()
        elif form_type == 'agent' and value == 3:
            form.update(responded=datetime.now(), status=3)
            user_ext = get_object_or_404(UserExt, user=form.first().user_id)
            user_ext.user_type = 1
            user_ext.save()
        elif form_type == 'general' or form_type == 'project':
            if value:
                form.update(responded=datetime.now())
            else:
                form.update(responded=None)
        return HttpUtils.FormattedResponse(status_code=status.HTTP_204_NO_CONTENT)
    return HttpUtils.FormattedResponse(status_code=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes([])
@authentication_classes([authentication.JWTAuthentication])
def external_update_project_form_status(request):
    form_id = request.data.get('id') or None
    responded = request.data.get('responded') or None

    if not form_id:
        return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed'
                                           , error_message='no form_id is supplied.', error_code='400')
    form = ProjectContactForm.objects.filter(id=form_id)
    if form.exists():
        if responded:
            form.update(responded=timezone.now())
        else:
            form.update(responded=None)
        return HttpUtils.FormattedResponse(status_code=status.HTTP_200_OK)

    return HttpUtils.FormattedResponse(status_code=status.HTTP_404_NOT_FOUND, status_text='failed'
                                       , error_message='form_id not found.', error_code='404')
