from django.apps import AppConfig


class FormsConfig(AppConfig):
    name = 'commonrealty.apps.forms'
