from django.urls import path

from commonrealty.apps.articles.admin_views import get_admin_article_list, get_admin_article_details, post_admin_article
from commonrealty.apps.articles.views import get_article_list, get_article_detail

app_name = 'articles'

urlpatterns = [
    path('', get_article_list, name='get_article_list'),
    path('admin/', get_admin_article_details, name='get_admin_article_details'),
    path('admin/post/', post_admin_article, name='post_admin_article'),
    path('admin/list/', get_admin_article_list, name='get_admin_article_list'),
    path('<str:article_type>/<str:article_slug>/', get_article_detail, name='get_article_detail')
]