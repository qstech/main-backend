from django.core.paginator import Paginator
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from commonrealty.apps.articles.models import Article, ArticleTag


@api_view(['POST'])
def get_article_list(request):
    page = request.data.get('page') if request.data.get('page') else 1
    article_type = request.data.get('type') if request.data.get('type') else 'media'
    article_tag = request.data.get('tag') or None
    items_per_page = request.data.get('items_per_page') if request.data.get('items_per_page') else 4

    articles = Article.objects.filter(publish_status=1, type__slug=article_type)

    if article_tag:
        articles = articles.filter(tag__slug=article_tag)

    articles = articles.order_by('-updated', '-created')
    paginator = Paginator(articles, items_per_page)
    output = {
        'total': paginator.num_pages,
        'current': page,
        'tag': ArticleTag.objects.get(slug=article_tag).name if article_tag else '',
        'items': map(lambda item: item.list_item_serializer(), paginator.get_page(page))
    }

    return Response(output, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_article_detail(request, article_type, article_slug):
    article = get_object_or_404(Article, type__slug=article_type, slug=article_slug, publish_status=1)
    article.views += 1
    article.save()
    return Response(article.detail_item_serializer(), status=status.HTTP_200_OK)
