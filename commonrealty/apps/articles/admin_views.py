from django.core.paginator import Paginator
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.generics import get_object_or_404
from rest_framework_simplejwt import authentication

from commonrealty.apps.articles.models import Article, ArticleTag
from commonrealty.apps.users.custom_permission import IsAuthenticatedAndInternal
from commonrealty.apps.utils import HttpUtils, GeneralUtils, ImageUtils


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_article_list(request):
    per_page = request.data.get('pageSize') or 10
    page = request.data.get('current') or 1
    filters = request.data.get('filter') or None
    sorter = request.data.get('sorter') or 'id'
    sorter_direction = request.data.get('sorterDirection') or ''

    articles = Article.objects.all()

    if filters:
        filter_options = {}
        for i in filters:
            value = map(lambda item: int(item), filters[i]) if filters[i] else None
            if value:
                if i == 'created_by':
                    filter_options['created_by_id__in'] = value
                if i == 'tags':
                    filter_options['tag__id__in'] = value
                if i == 'type':
                    filter_options['type_id__in'] = value
                if i == 'publish_status':
                    filter_options['publish_status__in'] = value
        articles = articles.filter(**filter_options)

    articles = articles.order_by('{}{}'.format(sorter_direction, sorter))

    paginator = Paginator(articles, per_page)

    response = {
        'total': articles.count(),
        'current': page,
        'per_page': per_page or 10,
        'total_pages': paginator.num_pages,
        'items': map(lambda item: item.list_item_serializer_admin(), paginator.get_page(page)),
        'filters': Article.filters_admin()
    }

    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_article_details(request):
    article_id = request.GET.get('articleId')

    response = {
        'options': Article.filters_admin(),
        'data': None
    }

    if article_id and article_id != 'new':
        article = get_object_or_404(Article, id=article_id)
        response['data'] = article.detail_item_serializer_admin()

    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['POST', 'DELETE'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def post_admin_article(request):
    article_id = request.GET.get('articleId')
    if request.method == 'POST':
        form_data = request.data

        if article_id:
            article = get_object_or_404(Article, id=article_id)
        else:
            article = Article(created_by_id=request.user.id)

        if form_data:
            if GeneralUtils.value_in_dict_and_not_null(form_data, 'title'):
                article.title = form_data.get('title')
                article.slug = form_data.get('title')
            if GeneralUtils.value_in_dict_and_not_null(form_data, 'body'):
                article.body = form_data.get('body')
            if GeneralUtils.value_in_dict_and_not_null(form_data, 'created'):
                article.created = form_data.get('created')
            if GeneralUtils.value_in_dict_and_not_null(form_data, 'type'):
                article.type_id = form_data.get('type')
            if GeneralUtils.value_in_dict_and_not_null(form_data, 'publish_status'):
                article.publish_status = form_data.get('publish_status')
            if GeneralUtils.value_in_dict_and_not_null(form_data, 'cover_image'):
                file = ImageUtils.covert_base64_file(form_data.get('cover_image'))
                article.cover_image.save(name=file['name'], content=file['content'])
            article.save()
            if GeneralUtils.value_in_dict_and_not_null(form_data, 'tags'):
                tags = []
                for tag in form_data.get('tags'):
                    if type(tag) == str:
                        new_tag = ArticleTag(name=tag, slug=tag).save()
                        tags.append(new_tag.id)
                    tags.append(tag)
                article.tag.set(tags)

        else:
            return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed', error_message='post body is not valid')

        return HttpUtils.FormattedResponse(data=article.id, status_code=status.HTTP_201_CREATED)

    if request.method == 'DELETE':
        if article_id:
            article = get_object_or_404(Article, id=article_id)
            article.delete()
            return HttpUtils.FormattedResponse(status_code=status.HTTP_204_NO_CONTENT)
        return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed')



