from django.apps import AppConfig


class ArticlesConfig(AppConfig):
    name = 'commonrealty.apps.articles'
