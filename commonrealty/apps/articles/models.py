import urllib.parse
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from commonrealty.apps.projects.models import ProjectDeveloper, Project
from commonrealty.apps.utils import ModelUtils


class ArticleType(models.Model):
    name = models.CharField(max_length=300)
    slug = models.CharField(max_length=300)

    def __str__(self):
        return self.name

    def get_article_count(self):
        return self.articles.count()
    get_article_count.short_description = 'Article Count'

    def save(self, *args, **kwargs):
        self.slug = ModelUtils.transform_field_into_slug(self.name)
        super().save(*args, **kwargs)


class ArticleTag(models.Model):
    name = models.CharField(max_length=300)
    slug = models.CharField(max_length=300)

    def __str__(self):
        return self.name

    def get_article_count(self):
        return self.articles.count()
    get_article_count.short_description = 'Article Count'

    def save(self, *args, **kwargs):
        self.slug = ModelUtils.transform_field_into_slug(self.name)
        super().save(*args, **kwargs)


class Article(models.Model):
    title = models.CharField(max_length=300)
    slug = models.CharField(unique=True, max_length=255)
    body = models.TextField(null=True, blank=True)
    cover_image = models.ImageField(upload_to='articles/', null=True, blank=True)
    type = models.ForeignKey(
        ArticleType,
        related_name='articles',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    tag = models.ManyToManyField(
        ArticleTag,
        related_name='articles',
        blank=True
    )
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='posted_articles',
        null=True,
        blank=True
    )
    publish_status = models.IntegerField(choices=ModelUtils.publish_status_enum, default=1)
    views = models.IntegerField(default=0)
    related_developers = models.ManyToManyField(
        ProjectDeveloper,
        blank=True,
        related_name='articles'
    )
    related_projects = models.ManyToManyField(
        Project,
        blank=True,
        related_name='articles'
    )

    def __str__(self):
        return self.title

    def get_admin_type_name(self):
        return self.type.name
    get_admin_type_name.short_description = 'Article Type'

    def get_admin_tag_names(self):
        return ', '.join(map(lambda item: item.name, self.tag.all()))
    get_admin_tag_names.short_description = 'Article Tags'

    def save(self, *args, **kwargs):
        self.slug = ModelUtils.transform_field_into_slug(self.title)
        super().save(*args, **kwargs)

    def list_item_serializer(self):
        return {
            'title': self.title,
            'image': self.cover_image.url,
            'slug': urllib.parse.quote(self.slug)
        }

    def detail_item_serializer(self):
        return {
            'title': self.title,
            'slug': urllib.parse.quote(self.slug),
            'image': self.cover_image.url,
            'body': self.body
        }

    def list_item_serializer_admin(self):
        return {
            'id': self.id,
            'slug': self.slug,
            'title': self.title,
            'tags': [i.name for i in self.tag.all()],
            'type': self.type.name,
            'created': self.created,
            'created_by': self.created_by.get_full_name(),
            'views': self.views,
            'publish_status': ModelUtils.get_string_value_from_enum(ModelUtils.publish_status_enum, self.publish_status),
            'preview_path': '/{}/{}'.format(self.type.slug, self.slug)
        }

    def detail_item_serializer_admin(self):
        return {
            'id': self.id,
            'title': self.title,
            'body': self.body,
            'cover_image': self.cover_image.url if self.cover_image else None,
            'tags': [i.id for i in self.tag.all()],
            'type': self.type.name,
            'created': self.created,
            'updated': self.updated,
            'created_by': self.created_by_id,
            'views': self.views,
            'publish_status': self.publish_status,
            'preview_path': '/{}/{}'.format(self.type.slug, self.slug)
        }

    @staticmethod
    def filters_admin():
        return {
            'types': [
                {
                    'text': i.name,
                    'value': i.id
                }
                for i in ArticleType.objects.all()
            ],
            'tags': [
                {
                    'text': i.name,
                    'value': i.id
                }
                for i in ArticleTag.objects.all()
            ],
            'publish_status': [
                {
                    'text': i[1],
                    'value': i[0]
                }
                for i in ModelUtils.publish_status_enum
            ],
            'created_by': [
                {
                    'text': User.objects.get(id=i).get_full_name(),
                    'value': i
                }
                for i in Article.objects.values_list('created_by_id', flat=True).distinct()
            ]
        }