from django.contrib import admin
from commonrealty.apps.articles.models import Article, ArticleTag, ArticleType


class ArticleTagAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_article_count')


class ArticleTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_article_count')


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_admin_type_name', 'get_admin_tag_names')


admin.site.register(Article, ArticleAdmin)
admin.site.register(ArticleTag, ArticleTagAdmin)
admin.site.register(ArticleType, ArticleTypeAdmin)
