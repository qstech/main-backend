from rest_framework import permissions

from commonrealty.apps.users.models import UserExt


class IsAuthenticatedAndInternal(permissions.BasePermission):
    def has_permission(self, request, view):
        if not request.user.id:
            return False
        user = UserExt.objects.get(user_id=request.user.id)
        return user.user_type == 3
