from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from commonrealty.apps.users.models import UserExt, AgentClientRecord


class ExtensionInline(admin.StackedInline):
    model = UserExt
    can_delete = False
    fk_name = 'user'


class UserAdmin(BaseUserAdmin):
    inlines = (ExtensionInline, )


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(AgentClientRecord)
