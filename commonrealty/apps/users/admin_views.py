from django.contrib.auth import user_logged_in
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.generics import get_object_or_404
from rest_framework_simplejwt import authentication
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken

from commonrealty.apps.forms.models import ProjectContactForm, GeneralContactForm, AgentApplicationForm
from commonrealty.apps.projects.models import Project
from commonrealty.apps.users.custom_permission import IsAuthenticatedAndInternal
from commonrealty.apps.users.models import UserExt
from commonrealty.apps.utils import HttpUtils, ImageUtils


@api_view(['POST'])
def admin_login(request):
    username = request.data.get('username') or None
    password = request.data.get('password') or None

    if not username or not password:
        return HttpUtils.FormattedResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            error_message='Username or password not provided',
            status_text='failed'
        )

    try:
        user = User.objects.get(username=username)
        if user.check_password(password) and user.ext.user_type == 3:
            refresh = RefreshToken.for_user(user)
            access = AccessToken.for_user(user)
            return HttpUtils.FormattedResponse(data={
                'refresh_token': str(refresh),
                'access_token': str(access),
                **user.ext.login_item_serializer_admin()
            })
        else:
            return HttpUtils.FormattedResponse(
                status_code=status.HTTP_401_UNAUTHORIZED,
                status_text='failed',
                error_message='Auth mismatch'
            )
    except User.DoesNotExist:
        return HttpUtils.FormattedResponse(
            status_code=status.HTTP_401_UNAUTHORIZED,
            status_text='failed',
            error_message='Auth mismatch'
        )


@api_view(['GET'])
@authentication_classes([authentication.JWTAuthentication])
def get_current_user(request):
    if request.user.id:
        try:
            user = User.objects.get(id=request.user.id)
            if user.is_active and user.ext.user_type == 3:
                user_logged_in.send(sender=user.__class__, request=request, user=user)
                return HttpUtils.FormattedResponse(
                    status_text='success',
                    data=user.ext.login_item_serializer_admin()
                )
            else:
                pass
        except User.DoesNotExist:
            pass
    return HttpUtils.FormattedResponse(
        status_code=status.HTTP_401_UNAUTHORIZED,
        status_text='failed',
        error_message='Auth mismatch'
    )


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def admin_user_profile_views(request):
    user = get_object_or_404(User, id=request.user.id)
    if request.method == 'GET':
        return HttpUtils.FormattedResponse(data=user.ext.profile_item_serializer_admin())
    data = request.data
    user_ext = get_object_or_404(UserExt, user_id=user.id)
    if 'first_name' in data:
        user.first_name = data.get('first_name')
    if 'last_name' in data:
        user.last_name = data.get('last_name')
    if 'phone' in data:
        user_ext.phone = data.get('phone')
    if 'avatar' in data:
        file = ImageUtils.covert_base64_file(data.get('avatar'))
        user_ext.avatar.save(name=file['name'], content=file['content'])
    user.save()
    user_ext.save()
    return HttpUtils.FormattedResponse(status_code=status.HTTP_202_ACCEPTED)


@api_view(['GET'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_current_user_dashboard(request):
    user = get_object_or_404(User, id=request.user.id)

    def get_total_project_view_counts():
        total_views = 0
        projects = Project.objects.all()
        for project in projects:
            total_views += project.views
            total_views += project.unauthenticated_views
        return total_views

    def get_total_enquiries_counts():
        project_forms = ProjectContactForm.objects.all().count()
        general_forms = GeneralContactForm.objects.all().count()
        agent_forms = AgentApplicationForm.objects.all().count()
        return project_forms + general_forms + agent_forms

    output = {
        'user': {
            'first_name': user.first_name,
            'last_login': user.last_login,
        },
        'projects': {
            'count': Project.objects.all().count(),
            'comparison': {
                'period': 'month',
                'difference': 0
            },
        },
        'clients': {
            'count': User.objects.filter(ext__user_type__in=[1,2]).count(),
            'comparison': {
                'period': 'month',
                'difference': 3
            },
        },
        'views': {
            'count': get_total_project_view_counts(),
            'comparison': {
                'period': 'month',
                'difference': 10
            },
        },
        'enquiries': {
            'count': get_total_enquiries_counts(),
            'comparison': {
                'period': 'month',
                'difference': -2
            },
        }
    }
    return HttpUtils.FormattedResponse(data=output)


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_client_list(request):
    per_page = request.data.get('pageSize') or 10
    page = request.data.get('current') or 1
    filters = request.data.get('filter') or None
    sorter = request.data.get('sorter') or 'id'
    sorter_direction = request.data.get('sorterDirection') or '-'

    clients = User.objects.filter(ext__user_type__in=[1, 2])

    if filters:
        filter_options = {}
        for i in filters:
            value = map(lambda item: int(item), filters[i]) if filters[i] else None
            if value:
                if i == 'user_type':
                    filter_options['ext__user_type__in'] = value
                elif i == 'user_status':
                    filter_options['is_active__in'] = value
                else:
                    filter_options[i + '__in'] = value
        clients = clients.filter(**filter_options)

    clients = clients.order_by('{}{}{}'.format(sorter_direction,
                                               'ext__' if sorter in ['language', 'phone', 'source', 'company'] else '',
                                               sorter))
    paginator = Paginator(clients, per_page)

    response = {
        'total': clients.count(),
        'current': page,
        'per_page': per_page or 10,
        'total_pages': paginator.num_pages,
        'items': map(lambda item: item.ext.client_list_item_serializer_admin(), paginator.get_page(page)),
        'filters': UserExt.filters_admin()
    }

    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_client_edit_form_data(request):
    data = {
        'data': None,
        'options': UserExt.form_options_admin()
    }
    user_id = request.GET.get('client_id')
    if user_id and user_id != 0:
        query_set = UserExt.objects.filter(user_id=user_id)
        if query_set.exists():
            data['data'] = query_set.first().form_item_serializer_admin()
    return HttpUtils.FormattedResponse(data=data, status_code=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_client_info_data(request):
    user_id = request.GET.get('client_id')
    if user_id and user_id != 0:
        query_set = UserExt.objects.filter(user_id=user_id)
        if query_set.exists():
            data = query_set.first().client_info_item_serializer_admin()
            return HttpUtils.FormattedResponse(data=data, status_code=status.HTTP_200_OK)

    return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed')



@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def post_client_form(request):
    if request.method == 'PUT':
        instance = get_object_or_404(User, id=request.data.get('id'))
        ext_instance = get_object_or_404(UserExt, user=instance)
        if 'user_status' in request.data:
            instance.is_active = request.data.get('user_status')
        if 'user_type' in request.data:
            ext_instance.user_type = request.data.get('user_type')
        ext_instance.save()
        instance.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_202_ACCEPTED)
    if request.method == 'POST':
        data = request.data
        if not request.data.get('email') or not request.data.get('first_name') or not request.data.get('last_name'):
            return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed')

        user_instance = User.objects.create_user(
            username=request.data.get('email'),
            email=request.data.get('email'),
            first_name=request.data.get('first_name'),
            last_name=request.data.get('last_name'),
            is_active=request.data.get('user_status') or False
        )
        user_instance.save()
        data.pop('email', None)
        data.pop('first_name', None)
        data.pop('last_name', None)
        data.pop('user_status', None)
        data.pop('id', None)
        user_ext_instance = UserExt(**data, user_id=user_instance.id)
        user_ext_instance.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_201_CREATED)
    if request.method == 'DELETE':
        instance = get_object_or_404(User, id=request.data.get('id'))
        instance.delete()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_204_NO_CONTENT)

    return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed', error_code='400')


# @api_view(['POST'])
# @permission_classes([IsAuthenticatedAndInternal])
# @authentication_classes([authentication.JWTAuthentication])
# def get_user_list(request):
#     per_page = request.data.get('pageSize') or 10
#     page = request.data.get('current') or 1
#     filters = request.data.get('filter') or None
#     sorter = request.data.get('sorter') or 'id'
#     sorter_direction = request.data.get('sorterDirection') or ''
#     integration = request.data.get('integration') or None
#
#     projects = Project.objects.all()
#
#     if filters:
#         filter_options = {}
#         for i in filters:
#             value = map(lambda item: int(item), filters[i]) if filters[i] else None
#             if value:
#                 filter_options[i + '__in'] = value
#         projects = Project.objects.filter(**filter_options)
#
#     projects = projects.order_by('{}{}'.format(sorter_direction, sorter))
#
#     paginator = Paginator(projects, per_page)
#
#     response = {
#         'total': projects.count(),
#         'current': page,
#         'per_page': per_page or 10,
#         'total_pages': paginator.num_pages,
#         'items': map(lambda item: item.integration_item_serializer() if integration else item.list_item_serializer_admin(), paginator.get_page(page)),
#         'filters': Project.filters_admin()
#     }
#
#     return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)