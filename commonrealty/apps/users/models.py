from django.contrib.auth.models import User
from django.db import models
from django.db.models.functions import datetime

from commonrealty.apps.utils import ModelUtils


class UserExt(models.Model):
    user_type_enum = (
        (0, 'Unknown'),
        (1, 'Standard'),
        (2, 'Agent'),
        (3, 'Internal')
    )

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='ext'
    )
    user_type = models.IntegerField(choices=user_type_enum, default=1)
    avatar = models.ImageField(upload_to='users/avatar/', null=True, blank=True)
    phone = models.CharField(max_length=20)
    country_of_residence = models.CharField(null=True, blank=True, max_length=255)
    interest = models.CharField(null=True, blank=True, max_length=255)
    source = models.CharField(null=True, blank=True, max_length=255)
    language = models.CharField(null=True, blank=True, max_length=255)
    wechat = models.CharField(null=True, blank=True, max_length=255)
    whatsapp = models.CharField(null=True, blank=True, max_length=255)
    company = models.CharField(null=True, blank=True, max_length=255)
    role = models.CharField(null=True, blank=True, max_length=255)
    industry = models.CharField(null=True, blank=True, max_length=255)
    project_view_counts = models.JSONField(default=dict)
    agent = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='sub_clients'
    )

    def __str__(self):
        return self.user.get_full_name()

    def detail_item_serializer(self):
        return {
            'id': self.id,
            'username': self.user.username,
            'full_name': self.user.get_full_name(),
            'email': self.user.email,
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'user_type': ModelUtils.get_string_value_from_enum(self.user_type_enum, self.user_type),
            'phone': self.phone,
            'country': self.country_of_residence,
            'interest': self.interest,
            'source': self.source,
            'language': self.language,
            'wechat': self.wechat,
            'whatsapp': self.whatsapp,
            'company': self.company,
            'role': self.role,
            'industry': self.interest,
            'clients': [
                {
                    'name': i.user.get_full_name(),
                    'last_login': i.user.last_login,
                    'created': i.user.date_joined,
                    'email': i.user.email
                }
                for i in self.user.sub_clients.all()
            ]
        }

    def profile_item_serializer_admin(self):
        return {
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'email': self.user.email,
            'phone': self.phone,
            'avatar': self.avatar.url if self.avatar else None
        }

    def login_item_serializer_admin(self):
        return {
            'name': self.user.get_full_name(),
            'access': 'admin' if self.user_type == 3 else 'user',
            'avatar': self.avatar.url if self.avatar else None,
            'userId': self.user_id
        }

    def client_list_item_serializer_admin(self):
        return {
            'id': self.user.id,
            'email': self.user.email,
            'user_type': ModelUtils.get_string_value_from_enum(self.user_type_enum, self.user_type),
            'user_status': {
                'text': 'Active' if self.user.is_active else 'Inactive',
                'color': '#87d068' if self.user.is_active else '#f50'
            },
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'phone': self.phone,
            'country': self.country_of_residence,
            'interest': self.interest,
            'source': self.source,
            'language': self.language,
            'wechat': self.wechat,
            'whatsapp': self.whatsapp,
            'company': self.company,
            'role': self.role,
            'industry': self.interest,
            'views': self.project_view_counts,
            'last_login': self.user.last_login,
            'date_joined': self.user.date_joined,

        }

    def client_info_item_serializer_admin(self):
        if self.user_type == 1:
            return {
                'user_type': ModelUtils.get_string_value_from_enum(self.user_type_enum, self.user_type),
                'current_agent': {
                    'name': self.agent.get_full_name() if self.agent else None,
                    'id': self.agent.id if self.agent else None,
                    'since': self.user.by_agent_change_records.filter(is_active=True).order_by(
                        '-created').first().created if self.agent else None
                },
                'by_agent_records': [
                    {
                        'agent_name': agent.agent.get_full_name(),
                        'agent_id': agent.agent.id,
                        'created': agent.created,
                        'withdrawn': agent.withdrawn,
                        'is_active': agent.is_active
                    } for agent in self.user.by_agent_change_records.all()
                ],
                # 'by_agent_request_forms': [
                #     {
                #         'requested_by_id': form.agent.id,
                #         'requested_by_name': form.agent.get_full_name(),
                #         'requested': form.created,
                #         'responded': form.responded,
                #         'status': form.status
                #     }
                #     for form in self.user.by_agent_request_applications.all()
                # ],
                'project_views': self.project_view_counts
            }
        return {
            'user_type': ModelUtils.get_string_value_from_enum(self.user_type_enum, self.user_type),
            'sub_clients': [
                {
                    'name': user.user.get_full_name(),
                    'id': user.user.id,
                    'since': user.user.by_agent_change_records.filter(is_active=True).order_by('-created').first().created
                } for user in self.user.sub_clients.all()
            ],
            'for_sub_client_records': [
                {
                    'sub_user_name': user.sub_client.get_full_name(),
                    'sub_user_id': user.sub_client.id,
                    'created': user.created,
                    'withdrawn': user.withdrawn,
                    'is_active': user.is_active
                } for user in self.user.for_sub_client_change_records.all()
            ],
            # 'for_sub_client_request_forms': [
            #     {
            #         'requested_for_id': form.sub_client_user.id if form.sub_client_user else None,
            #         'requested_for_name': form.sub_client_user.get_full_name()
            #         if form.sub_client_user else form.sub_client_name,
            #         'requested': form.created,
            #         'responded': form.responded,
            #         'status': form.status
            #     }
            #     for form in self.user.for_sub_client_applications.all()
            # ],
            'project_views': self.project_view_counts
        }

    def form_item_serializer_admin(self):
        return {
            'id': self.user.id,
            'user_type': self.user_type,
            'user_status': self.user.is_active,
            'email': self.user.email,
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'phone': self.phone,
            'country_of_residence': self.country_of_residence,
            'interest': self.interest,
            'source': self.source,
            'language': self.language,
            'wechat': self.wechat,
            'whatsapp': self.whatsapp,
            'company': self.company,
            'role': self.role,
            'industry': self.interest,
            'last_login': self.user.last_login,
            'date_joined': self.user.date_joined
        }

    @staticmethod
    def form_options_admin():
        return {
            'user_type': list(UserExt.user_type_enum)[1:3]
        }

    @staticmethod
    def filters_admin():
        return {
            'user_status': list(((0, 'Inactive'), (1, 'Active'))),
            'user_type': list(UserExt.user_type_enum)[1:3]
        }


class AgentClientRecord(models.Model):
    agent = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='for_sub_client_change_records'
    )
    sub_client = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='by_agent_change_records'
    )
    created = models.DateTimeField(default=datetime.timezone.now)
    is_active = models.BooleanField(default=True)
    withdrawn = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '{} => {}'.format(self.agent.get_full_name(), self.sub_client.get_full_name())