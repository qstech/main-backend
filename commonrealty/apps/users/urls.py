from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from commonrealty.apps.users.custom_token import CustomTokenObtainPairView
from commonrealty.apps.users.views import get_user, update_user, register_user
from commonrealty.apps.users.admin_views import admin_login, get_current_user, get_client_list,\
    get_admin_client_edit_form_data, post_client_form, get_admin_current_user_dashboard, admin_user_profile_views,\
    get_admin_client_info_data

app_name = 'users'

urlpatterns = [
    path('token/', CustomTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('admin-token/', admin_login, name='admin_login'),
    path('current/', get_current_user, name='get_current_user'),
    path('', get_user, name='get_user'),
    path('update/', update_user, name='update_user'),
    path('register/', register_user, name='register_user'),
    # admin
    path('admin/dashboard/', get_admin_current_user_dashboard, name='get_admin_current_user_dashboard'),
    path('admin/profile/', admin_user_profile_views, name='admin_user_profile_views'),
    path('admin/client/post/', post_client_form, name='post_client_form'),
    path('admin/client/list/', get_client_list, name='get_client_list'),
    path('admin/client/edit-form-data/', get_admin_client_edit_form_data,
         name='get_admin_client_form_options'),
    path('admin/client/info/', get_admin_client_info_data, name='get_admin_client_info_data')
]
