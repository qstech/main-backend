from datetime import datetime

from django.contrib.auth.models import User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from commonrealty.apps.users.models import UserExt
from commonrealty.apps.utils import ModelUtils


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        user_ext = UserExt.objects.get(user=user)
        user_obj = User.objects.get(id=user.id)
        user_obj.last_login = datetime.now()
        user_obj.save()
        token['user_type'] = ModelUtils.get_string_value_from_enum(UserExt.user_type_enum, user_ext.user_type)
        token['name'] = user.get_full_name()
        return token


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer
