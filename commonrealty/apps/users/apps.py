from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'commonrealty.apps.users'
