import requests
from commonrealty import settings
from django.contrib.auth import user_logged_in
from django.contrib.auth.models import User
from rest_framework import status, permissions
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework_simplejwt import authentication
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken

from commonrealty.apps.users.models import UserExt


# Internal Methods
from commonrealty.apps.utils import HttpUtils


def create_user(email, phone, password, first_name=None, last_name=None, *args, **kwargs):
    user = User.objects.create_user(
        email=email,
        username=email,
        password=password,
        first_name=first_name,
        last_name=last_name
    )
    if user.id:
        user_ext = UserExt(user_id=user.id, phone=phone, *args, **kwargs)
        user_ext.save()
        return user.id
    return False


@api_view(['POST'])
def register_user(request):
    first_name = request.data.get('first_name')
    last_name = request.data.get('last_name')
    email = request.data.get('email')
    password = request.data.get('password')
    phone = request.data.get('phone')
    user_id = create_user(email=email, phone=phone, password=password, first_name=first_name, last_name=last_name)

    if user_id:
        req = requests.post(settings.SALESFORCE_TOKEN_URL)
        auth_header = '{} {}'.format(req.json().get('token_type'), req.json().get('access_token'))
        data = request.data
        data['password'] = ''
        data['user_id'] = user_id
        lead_sync_req = requests.post(settings.SALESFORCE_LEAD_SYNC_URL, json=data,
                                      headers={"Authorization": auth_header})
        if lead_sync_req.status_code == 200:
            return Response(status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response(status=status.HTTP_400_BAD_REQUEST)


# Endpoint Methods
@api_view(['GET'])
@authentication_classes([authentication.JWTAuthentication])
@permission_classes([permissions.IsAuthenticated])
def get_user(request):
    user = get_object_or_404(User, id=request.user.id)
    user_logged_in.send(sender=user.__class__, request=request, user=user)
    return Response(user.ext.detail_item_serializer(), status=status.HTTP_200_OK)


@api_view(['POST'])
@authentication_classes([authentication.JWTAuthentication])
@permission_classes([permissions.IsAuthenticated])
def update_user(request):
    user = get_object_or_404(User, id=request.user.id)
    user_ext = get_object_or_404(UserExt, user_id=request.user.id)
    data = request.data
    if 'first_name' in data:
        user.first_name = data.get('first_name')
    if 'last_name' in data:
        user.last_name = data.get('last_name')
    if 'phone' in data:
        user_ext.phone = data.get('phone')
    if 'wechat' in data:
        user_ext.wechat = data.get('wechat')
    if 'whatsapp' in data:
        user_ext.whatsapp = data.get('whatsapp')
    if 'language' in data:
        user_ext.language = data.get('language')
    if 'country' in data:
        user_ext.country_of_residence = data.get('country')
    if 'interest' in data:
        user_ext.interest = data.get('interest')
    if 'source' in data:
        user_ext.source = data.get('source')
    user.save()
    user_ext.save()
    return Response(status=status.HTTP_200_OK)