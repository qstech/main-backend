from django.core.paginator import Paginator
from django.db.models import Q
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework_simplejwt import authentication

from commonrealty.apps.projects.models import Project
from commonrealty.apps.users.models import UserExt
from commonrealty.apps.utils import ModelUtils


@api_view(['POST'])
@authentication_classes([authentication.JWTAuthentication])
def get_project_list(request):
    map_list = request.data.get('map') if request.data.get('map') else False
    map_boundary = request.data.get('map_boundary') or False
    home_page_request = request.data.get('home') or False
    page = request.data.get('page') or 0
    items_per_page = request.data.get('items_per_page') or 0

    bedroom = request.data.get('bedroom') if request.data.get('bedroom') else 'all'
    bathroom = request.data.get('bathroom') if request.data.get('bathroom') else 'all'
    car_space = request.data.get('car_space') if request.data.get('car_space') else 'all'
    property_type = request.data.get('property_type') if request.data.get('property_type') else 'all'
    min_price = int(request.data.get('min_price')) if request.data.get('min_price') else 0
    max_price = int(request.data.get('max_price')) if request.data.get('max_price') else 0
    promotion = request.data.get('promotion') if request.data.get('promotion') else 'all'
    search = request.data.get('search') if request.data.get('search') else None

    investment_options = request.data.get('investment_options')
    completion_options = request.data.get('completion_options')

    projects = Project.objects.filter(publish_status=1)
    total_count = projects.count()

    if search:
        projects = projects.filter(address__icontains=search)

    if promotion != 'all':
        projects = projects.filter(promotion_status=promotion)

    if investment_options and len(investment_options) > 0:
        projects = projects.filter(attributes__slug__in=investment_options)

    if completion_options and len(completion_options) > 0:
        or_relation = Q()
        for option in completion_options:
            if option[0] == 0 and len(option) == 1:
                or_relation.add(Q(completion_date__lte=timezone.now()), Q.OR)
            elif option[0] == 24 and len(option) == 1:
                or_relation.add(Q(completion_date__gte=ModelUtils.get_future_datetime_from_delta(24)), Q.OR)
            else:
                or_relation.add(Q(completion_date__range=(ModelUtils.get_future_datetime_from_delta(option[0]),
                                                          ModelUtils.get_future_datetime_from_delta(option[1]))), Q.OR)
        projects = projects.filter(or_relation)

    suite_options = {}

    if bedroom != 'all':
        if bedroom == '5+':
            suite_options['bedroom__gte'] = 5
            # projects = projects.filter(suites__bedroom__gte=5).distinct()
        else:
            suite_options['bedroom'] = bedroom
            # projects = projects.filter(suites__bedroom=bedroom).distinct()

    if bathroom != 'all':
        if bathroom == '3+':
            suite_options['bathroom__gte'] = 3
            # projects = projects.filter(suites__bathroom__gte=3).distinct()
        else:
            suite_options['bathroom'] = bathroom
            # projects = projects.filter(suites__bathroom=bathroom).distinct()

    if car_space != 'all':
        if car_space == '3+':
            suite_options['car_space__gte'] = 3
            # projects = projects.filter(suites__car_space__gte=3).distinct()
        else:
            suite_options['car_space'] = car_space
            # projects = projects.filter(suites__car_space=car_space).distinct()

    if property_type != 'all':
        suite_options['suite_type'] = property_type
        # projects = projects.filter(suites__suite_type=property_type).distinct()

    if min_price > 0:
        suite_options['min_price__gte'] = min_price
        # projects = projects.filter(suites__max_price__gte=min_price).distinct()

    if max_price > 0:
        suite_options['max_price__lte'] = max_price
        # projects = projects.filter(suites__min_price__lte=max_price).distinct()

    if (not request.user.id) and (not map_list):
        projects = projects.filter(visible_to_non_login=True)

    projects = projects.order_by('-priority_level', '-updated', '-created')

    filtered_projects = []
    for option in suite_options:
        if suite_options[option] is None:
            del(suite_options[option])

    for i in projects:
        if i.suites.filter(**suite_options).count() > 0:
            filtered_projects.append(i)

    if map_boundary:
        output_list = []
        for i in filtered_projects:
            if (map_boundary['lat']['min'] < i.coordinates['lat'] < map_boundary['lat']['max']) and (map_boundary['lng']['min'] < i.coordinates['lng'] < map_boundary['lng']['max']):
                output_list.append(i)
        filtered_projects = output_list

    if items_per_page and page:
        filtered_projects = Paginator(filtered_projects, items_per_page).get_page(page)

    if home_page_request:
        output = {
            'total': total_count,
            'items': map(lambda item: item.list_item_serializer(map_list=map_list, request=request), filtered_projects)
        }
    else:
        output = map(lambda item: item.list_item_serializer(map_list=map_list, request=request), filtered_projects)

    return Response(output, status=status.HTTP_200_OK)


@api_view(['GET'])
@authentication_classes([authentication.JWTAuthentication])
def get_project_detail(request, project_slug):
    map_view = request.GET.get('map') if request.GET.get('map') else False
    project = get_object_or_404(Project, slug=project_slug, publish_status=1)

    if (not request.user.id) and project.visible_to_non_login:
        project.unauthenticated_views += 1
    elif request.user.id:
        project.views += 1
        user = UserExt.objects.filter(user_id=request.user.id)
        if user.exists():
            project_view_dict = user.first().project_view_counts
            if project_view_dict.get(str(project.id)):
                project_view_dict[str(project.id)]['views'] += 1
            else:
                project_view_dict[str(project.id)] = {
                    'name': project.name,
                    'views': 1
                }
            user.update(project_view_counts=project_view_dict)
    project.save()
    if (not request.user.id) and (not project.visible_to_non_login):
        return Response(status=status.HTTP_404_NOT_FOUND)
    return Response(project.detail_item_serializer(map_view=map_view), status=status.HTTP_200_OK)
