# Generated by Django 3.1.3 on 2020-11-16 21:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0015_auto_20201116_2017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='promotion_status',
            field=models.IntegerField(choices=[(0, 'Unknown'), (1, 'On Promotion'), (2, 'Not On Promotion')], default=1),
        ),
    ]
