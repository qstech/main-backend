import csv
import io
from datetime import datetime

from django.core.paginator import Paginator
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework_simplejwt import authentication

from commonrealty.apps.projects.models import Project, ProjectDeveloper, ProjectBuilder, ProjectArchitect, \
    ProjectAttribute, ProjectSuburb, ProjectSuburbAsset
from commonrealty.apps.projects.serializers import ProjectAdminSerializer
from commonrealty.apps.users.custom_permission import IsAuthenticatedAndInternal
from commonrealty.apps.utils import HttpUtils, ImageUtils


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_project_list(request):
    per_page = request.data.get('pageSize') or 10
    page = request.data.get('current') or 1
    filters = request.data.get('filter') or None
    sorter = request.data.get('sorter') or 'id'
    sorter_direction = request.data.get('sorterDirection') or ''
    integration = request.data.get('integration') or None

    projects = Project.objects.all()

    if filters:
        filter_options = {}
        for i in filters:
            value = map(lambda item: int(item), filters[i]) if filters[i] else None
            if value:
                filter_options[i + '__in'] = value
        projects = Project.objects.filter(**filter_options)

    projects = projects.order_by('{}{}'.format(sorter_direction, sorter))

    paginator = Paginator(projects, per_page)

    response = {
        'total': projects.count(),
        'current': page,
        'per_page': per_page or 10,
        'total_pages': paginator.num_pages,
        'items': map(lambda item: item.integration_item_serializer() if integration else item.list_item_serializer_admin(), paginator.get_page(page)),
        'filters': Project.filters_admin()
    }

    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_project_edit_form_data(request):
    data = {
        'project_data': None,
        'options': Project.form_options_admin()
    }
    project_id = request.GET.get('project_id')
    if project_id and project_id != 0:
        project = Project.objects.filter(id=project_id)
        if project.exists():
            data['project_data'] = project.first().form_item_serializer_admin()

    return HttpUtils.FormattedResponse(data=data, status_code=status.HTTP_200_OK)


@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def post_project_form(request):
    serializer = None
    data = request.data
    ctx = {}
    if 'cover_image' in request.data:
        ctx['cover_image'] = request.data.get('cover_image')
    if 'suites' in request.data:
        ctx['suites'] = request.data.get('suites')
    if 'project_images' in request.data:
        ctx['project_images'] = request.data.get('project_images')
    if 'project_files' in request.data:
        ctx['project_files'] = request.data.get('project_files')
    if 'developers' in request.data:
        ctx['developers'] = request.data.get('developers')
    if 'builders' in request.data:
        ctx['builders'] = request.data.get('builders')
    if 'architects' in request.data:
        ctx['architects'] = request.data.get('architects')
    if 'attributes' in request.data:
        ctx['attributes'] = request.data.get('attributes')
    if 'suburb' in request.data:
        ctx['suburb'] = request.data.get('suburb')
    if request.method == 'POST':
        data['slug'] = data['name']
        serializer = ProjectAdminSerializer(data=data, context=ctx)
    if request.method == 'PUT':
        project = get_object_or_404(Project, id=request.GET.get('project_id'))
        ctx['project_id'] = project.id
        serializer = ProjectAdminSerializer(project, data=data, partial=True, context=ctx)
    if request.method == 'DELETE':
        project = get_object_or_404(Project, id=request.GET.get('project_id'))
        project.delete()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_204_NO_CONTENT)
    if not serializer or not serializer.is_valid():
        return HttpUtils.FormattedResponse(error_message=serializer.errors, status_code=status.HTTP_400_BAD_REQUEST)
    else:
        serializer.save()
    return HttpUtils.FormattedResponse(status_code=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes([])
@authentication_classes([])
def get_project_export_csv(request):
    projects = request.data.get('projects')

    if not projects:
        return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed')

    data = []
    for i in projects:
        project = Project.objects.filter(id=i)
        if project.exists():
            data.append(project.first().csv_item_serializer())

    output = io.StringIO()

    writer = csv.DictWriter(
        output, fieldnames=data[0].keys(), delimiter=",", quoting=csv.QUOTE_MINIMAL)
    writer.writeheader()
    writer.writerows(data)

    # return HttpUtils.FormattedResponse(status_code=status.HTTP_200_OK, data=output.getvalue())
    return Response(data=output.getvalue())


@api_view(['PUT'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def edit_project_view_integration(request):
    project_id = request.data.get('id') or None
    data = request.data
    data.pop('id', None)
    if not project_id:
        return HttpUtils.FormattedResponse(error_message='id not supplied', status_code=status.HTTP_400_BAD_REQUEST
                                           , error_code='400', status_text='failed')
    project = Project.objects.filter(id=project_id)
    if not project.exists():
        return HttpUtils.FormattedResponse(error_message='id not found', status_code=status.HTTP_404_NOT_FOUND
                                           , error_code='404', status_text='failed')
    project.update(**data)
    return HttpUtils.FormattedResponse(status_code=status.HTTP_206_PARTIAL_CONTENT)


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_organization_list(request):
    query = request.GET.get('query')
    per_page = request.data.get('pageSize') or 10
    page = request.data.get('current') or 1
    sorter = request.data.get('sorter') or 'id'
    sorter_direction = request.data.get('sorterDirection') or ''

    if query == 'architect':
        data = ProjectArchitect.objects.all()
    elif query == 'builder':
        data = ProjectBuilder.objects.all()
    else:
        data = ProjectDeveloper.objects.all()

    data = data.order_by('{}{}'.format(sorter_direction, sorter))

    paginator = Paginator(data, per_page)

    response = {
        'total': data.count(),
        'current': page,
        'per_page': per_page or 10,
        'total_pages': paginator.num_pages,
        'items': map(lambda item: item.list_item_serializer_admin(), paginator.get_page(page)),
    }
    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_organization_edit_form_data(request):
    query = request.GET.get('query')
    data = {
        'data': None,
    }
    organization_id = request.GET.get('organization_id')
    if organization_id and organization_id != 0:
        if query == 'architect':
            query_set = ProjectArchitect.objects.filter(id=organization_id)
        elif query == 'builder':
            query_set = ProjectBuilder.objects.filter(id=organization_id)
        else:
            query_set = ProjectDeveloper.objects.filter(id=organization_id)

        if query_set.exists():
            data['data'] = query_set.first().list_item_serializer_admin()

    return HttpUtils.FormattedResponse(data=data, status_code=status.HTTP_200_OK)


@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def post_organization_form(request):
    query = request.GET.get('query')
    if query == 'architect':
        query_set = ProjectArchitect
    elif query == 'builder':
        query_set = ProjectBuilder
    else:
        query_set = ProjectDeveloper
    if request.method == 'PUT':
        instance = get_object_or_404(query_set, id=request.data.get('id'))
        if 'desc' in request.data:
            instance.desc = request.data.get('desc')
        if 'name' in request.data:
            instance.name = request.data.get('name')
        if 'cover_image' in request.data:
            file = ImageUtils.covert_base64_file(request.data.get('cover_image'))
            instance.cover_image.save(name=file['name'], content=file['content'])
        instance.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_202_ACCEPTED)
    if request.method == 'POST':
        data = {
            'name': request.data.get('name'),
            'desc': request.data.get('desc') or ''
        }
        instance = query_set(**data)
        file = ImageUtils.covert_base64_file(request.data.get('cover_image'))
        instance.cover_image.save(name=file['name'], content=file['content'])
        instance.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_201_CREATED)
    if request.method == 'DELETE':
        instance = get_object_or_404(query_set, id=request.data.get('id'))
        instance.delete()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_204_NO_CONTENT)

    return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed', error_code='400')


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_attribute_list(request):
    per_page = request.data.get('pageSize') or 10
    page = request.data.get('current') or 1
    sorter = request.data.get('sorter') or 'id'
    sorter_direction = request.data.get('sorterDirection') or ''

    data = ProjectAttribute.objects.all()
    if 'filter' in request.data and len(request.data.get('filter')) > 0:
        filter_options = {}
        for i in request.data.get('filter'):
            if request.data.get('filter')[i]:
                filter_options[i + '__in'] = request.data.get('filter')[i]
        data = data.filter(**filter_options)

    data = data.order_by('{}{}'.format(sorter_direction, sorter))

    paginator = Paginator(data, per_page)
    response = {
        'total': data.count(),
        'current': page,
        'per_page': per_page or 10,
        'total_pages': paginator.num_pages,
        'filters': ProjectAttribute.filters_admin(),
        'items': map(lambda item: item.list_item_serializer_admin(), paginator.get_page(page)),
    }
    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_attribute_edit_form_data(request):
    data = {
        'data': None,
        'options': ProjectAttribute.form_options_admin()
    }
    attribute_id = request.GET.get('attribute_id')
    if attribute_id and attribute_id != 0:
        query_set = ProjectAttribute.objects.filter(id=attribute_id)
        if query_set.exists():
            data['data'] = query_set.first().list_item_serializer_admin()
    return HttpUtils.FormattedResponse(data=data, status_code=status.HTTP_200_OK)


@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def post_attribute_form(request):
    if request.method == 'PUT':
        instance = get_object_or_404(ProjectAttribute, id=request.data.get('id'))
        if 'category' in request.data:
            instance.desc = request.data.get('desc')
        if 'name' in request.data:
            instance.name = request.data.get('name')
        if 'icon' in request.data:
            file = ImageUtils.covert_base64_file(request.data.get('icon'))
            instance.icon.save(name=file['name'], content=file['content'])
        instance.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_202_ACCEPTED)
    if request.method == 'POST':
        data = {
            'name': request.data.get('name'),
            'category': request.data.get('category') or ''
        }
        instance = ProjectAttribute(**data)
        file = ImageUtils.covert_base64_file(request.data.get('icon'))
        instance.icon.save(name=file['name'], content=file['content'])
        instance.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_201_CREATED)
    if request.method == 'DELETE':
        instance = get_object_or_404(ProjectAttribute, id=request.data.get('id'))
        instance.delete()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_204_NO_CONTENT)

    return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed', error_code='400')


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_suburb_list(request):
    per_page = request.data.get('pageSize') or 10
    page = request.data.get('current') or 1
    filters = request.data.get('filter') or None
    sorter = request.data.get('sorter') or 'id'
    sorter_direction = request.data.get('sorterDirection') or ''

    data = ProjectSuburb.objects.all()

    if filters:
        filter_options = {}
        for i in filters:
            if request.data.get('filter')[i]:
                filter_options[i + '__in'] = request.data.get('filter')[i]
        data = data.filter(**filter_options)
    data = data.order_by('{}{}'.format(sorter_direction, sorter))

    paginator = Paginator(data, per_page)

    response = {
        'total': data.count(),
        'current': page,
        'per_page': per_page or 10,
        'total_pages': paginator.num_pages,
        'filters': ProjectSuburb.filters_admin(),
        'items': map(lambda item: item.list_item_serializer_admin(), paginator.get_page(page)),
    }
    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_suburb_edit_form_data(request):
    data = {
        'data': None,
        'options': ProjectSuburb.filters_admin()
    }
    suburb_id = request.GET.get('suburb_id')
    if suburb_id and suburb_id != 0:
        query_set = ProjectSuburb.objects.filter(id=suburb_id)
        if query_set.exists():
            data['data'] = query_set.first().form_item_serializer_admin()
    return HttpUtils.FormattedResponse(data=data, status_code=status.HTTP_200_OK)


@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def post_suburb_form(request):
    def suburb_images_handling(image_list):
        for i in image_list:
            if i.get('uid') == -1 and not i.get('deleted'):
                image = ProjectSuburbAsset(name=i.get('name') or str(datetime.now()), suburb_id=instance.id)
                image.type = 1
                file_content = ImageUtils.covert_base64_file(i.get('url'))
                image.file.save(name=file_content['name'], content=file_content['content'])
                image.save()
            if i.get('uid') != -1 and i.get('deleted'):
                image = ProjectSuburbAsset.objects.get(id=i.get('uid'))
                image.delete()

    if request.method == 'PUT':
        instance = get_object_or_404(ProjectSuburb, id=request.data.get('id'))
        if 'postcode' in request.data:
            instance.postcode = request.data.get('postcode')
        if 'region' in request.data:
            instance.region = request.data.get('region')
        if 'council_area' in request.data:
            instance.council_area = request.data.get('council_area')
        if 'name' in request.data:
            instance.name = request.data.get('name')
        if 'images' in request.data:
            suburb_images_handling(request.data.get('images'))
        instance.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_202_ACCEPTED)
    if request.method == 'POST':
        data = {
            'name': request.data.get('name'),
            'region': request.data.get('region') or '',
            'postcode': request.data.get('postcode'),
            'council_area': request.data.get('council_area')
        }
        instance = ProjectSuburb(**data)
        instance.save()
        if 'images' in request.data:
            suburb_images_handling(request.data.get('images'))
        return HttpUtils.FormattedResponse(status_code=status.HTTP_201_CREATED)
    if request.method == 'DELETE':
        instance = get_object_or_404(ProjectSuburb, id=request.data.get('id'))
        instance.delete()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_204_NO_CONTENT)

    return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed', error_code='400')


