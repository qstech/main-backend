from datetime import datetime

from rest_framework import serializers

from commonrealty.apps.projects.models import Project, ProjectSuite, ProjectAsset, ProjectDeveloper, ProjectBuilder, \
    ProjectArchitect, ProjectSuburb
from commonrealty.apps.utils import ImageUtils


class ProjectAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        exclude = ['cover_image', 'developers', 'architects', 'builders', 'attributes', 'suburb']

    def update_info(self, project):
        if self.context.get('cover_image'):
            file = ImageUtils.covert_base64_file(self.context.get('cover_image'))
            project.cover_image.save(name=file['name'], content=file['content'])
        if self.context.get('suites'):
            for i in self.context.get('suites'):
                if i.get('key') == -1:
                    suite = ProjectSuite.objects.get(id=i.get('id'))
                    if suite.id:
                        suite.delete()
                i.pop('key', None)
                i['min_price'] = i['min']
                i.pop('min', None)
                i['max_price'] = i['max']
                i.pop('max', None)

                if not i.get('id'):
                    suite = ProjectSuite(project_id=project.id, **i)
                    suite.save()
                else:
                    project.suites.filter(id=i['id']).update(**i)
        if 'project_images' in self.context:
            for i in self.context.get('project_images'):
                if i.get('uid') == -1 and not i.get('deleted'):
                    image = ProjectAsset(name=i.get('name') or str(datetime.now()), project_id=project.id)
                    image.type = 1
                    file_content = ImageUtils.covert_base64_file(i.get('url'))
                    image.file.save(name=file_content['name'], content=file_content['content'])
                    image.save()
                if i.get('uid') != -1 and i.get('deleted'):
                    image = ProjectAsset.objects.get(id=i.get('uid'))
                    image.delete()
        if 'project_files' in self.context:
            for i in self.context.get('project_files'):
                if i.get('uid') == -1 and not i.get('deleted'):
                    file = ProjectAsset(name=i.get('name') or str(datetime.now()), project_id=project.id)
                    file.type = 2
                    file_content = ImageUtils.covert_base64_file(i.get('url'))
                    file.file.save(name=file_content['name'], content=file_content['content'])
                    file.save()
                if i.get('uid') != -1 and i.get('deleted'):
                    file = ProjectAsset.objects.get(id=i.get('uid'))
                    file.delete()
        if 'developers' in self.context:
            developers = self.context.get('developers')
            for i, v in enumerate(developers):
                if type(v) is int:
                    developer = ProjectDeveloper.objects.filter(id=v)
                    if not developer.exists():
                        del developers[i]
                if type(v) is str:
                    developer = ProjectDeveloper(name=v)
                    developer.save()
                    if developer.id:
                        developers[i] = developer.id
            project.developers.set(developers)
        if 'builders' in self.context:
            builders = self.context.get('builders')
            for i, v in enumerate(builders):
                if type(v) is int:
                    builder = ProjectBuilder.objects.filter(id=v)
                    if not builder.exists():
                        del builders[i]
                if type(v) is str:
                    builder = ProjectBuilder(name=v)
                    builder.save()
                    if builder.id:
                        builders[i] = builder.id
            project.builders.set(builders)
        if 'architects' in self.context:
            architects = self.context.get('architects')
            for i, v in enumerate(architects):
                if type(v) is int:
                    architect = ProjectArchitect.objects.filter(id=v)
                    if not architect.exists():
                        del architects[i]
                if type(v) is str:
                    architect = ProjectArchitect(name=v)
                    architect.save()
                    if architect.id:
                        architects[i] = architect.id
            project.architects.set(architects)
        if 'suburb' in self.context:
            suburb = ProjectSuburb.objects.filter(name__iexact=self.context.get('suburb'))
            if suburb.exists():
                project.suburb_id = suburb.first().id
            else:
                new_suburb = ProjectSuburb(name=self.context.get('suburb'))
                new_suburb.save()
                project.suburb_id = new_suburb.id
            project.save()
        if 'attributes' in self.context:
            project.attributes.set(self.context.get('attributes'))

    def create(self, validated_data):
        project = Project(**validated_data)
        project.save()
        self.update_info(project)
        return project

    def update(self, instance, validated_data):
        project = Project.objects.filter(id=self.context.get('project_id'))
        project.update(**validated_data)
        self.update_info(project.first())
        return project
