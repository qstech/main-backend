from django.urls import path

from commonrealty.apps.projects.views import get_project_list, get_project_detail
from commonrealty.apps.projects.admin_views import get_admin_project_list, get_admin_project_edit_form_data, \
    post_project_form, get_admin_organization_list, get_admin_organization_edit_form_data, post_organization_form,\
    get_admin_attribute_list, get_admin_attribute_edit_form_data, post_attribute_form, get_admin_suburb_list, \
    get_admin_suburb_edit_form_data, post_suburb_form, edit_project_view_integration, get_project_export_csv

app_name = 'projects'

urlpatterns = [
    path('', get_project_list, name='get_project_list'),
    path('<str:project_slug>/', get_project_detail, name='get_project_detail'),
    # Admin
    path('admin/post/', post_project_form, name='post_project_form'),
    path('admin/integration-post/', edit_project_view_integration, name='edit_project_view_integration'),
    path('admin/csv/', get_project_export_csv, name='get_project_export_csv'),
    path('admin/list/', get_admin_project_list, name='get_admin_project_list'),
    path('admin/edit-form-data/', get_admin_project_edit_form_data, name='get_admin_project_form_options'),
    path('admin/organization/post/', post_organization_form, name='post_organization_form'),
    path('admin/organization/list/', get_admin_organization_list, name='get_admin_organization_list'),
    path('admin/organization/edit-form-data/', get_admin_organization_edit_form_data,
         name='get_admin_organization_form_options'),
    path('admin/attribute/post/', post_attribute_form, name='post_attribute_form'),
    path('admin/attribute/list/', get_admin_attribute_list, name='get_admin_attribute_list'),
    path('admin/attribute/edit-form-data/', get_admin_attribute_edit_form_data,
         name='get_admin_attribute_form_options'),
    path('admin/suburb/post/', post_suburb_form, name='post_suburb_form'),
    path('admin/suburb/list/', get_admin_suburb_list, name='get_admin_suburb_list'),
    path('admin/suburb/edit-form-data/', get_admin_suburb_edit_form_data,
         name='get_admin_suburb_form_options'),
]