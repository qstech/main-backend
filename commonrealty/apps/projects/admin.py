from django.contrib import admin
from commonrealty.apps.projects.models import Project, ProjectDeveloper, ProjectSuite, ProjectAsset, ProjectAttribute, ProjectBuilder, ProjectArchitect, ProjectSuburb, ProjectSuburbAsset


class SuitesInline(admin.TabularInline):
    model = ProjectSuite
    can_delete = True
    extra = 0


class AssetsInline(admin.TabularInline):
    model = ProjectAsset
    can_delete = True
    extra = 0


class ProjectAdmin(admin.ModelAdmin):
    inlines = (SuitesInline, AssetsInline)


class SuburbAssetsInline(admin.TabularInline):
    model = ProjectSuburbAsset
    can_delete = True
    extra = 0


class SuburbAdmin(admin.ModelAdmin):
    inlines = (SuburbAssetsInline, )


admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectDeveloper)
admin.site.register(ProjectBuilder)
admin.site.register(ProjectArchitect)
admin.site.register(ProjectSuburb, SuburbAdmin)
admin.site.register(ProjectAttribute)
