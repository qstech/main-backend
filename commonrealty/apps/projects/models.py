import urllib.parse

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from commonrealty.apps.utils import ModelUtils, AssetModelClass


class ProjectDeveloper(models.Model):
    name = models.CharField(max_length=255)
    cover_image = models.ImageField(upload_to='developers/', null=True, blank=True)
    desc = models.TextField(default='', blank=True)

    class Meta:
        verbose_name = 'Developer'
        verbose_name_plural = 'Developers'

    def __str__(self):
        return self.name

    def list_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': self.name,
            'cover_image': self.cover_image.url if self.cover_image else None,
            'projects': map(lambda item: {'name': item.name, 'id': item.id}, self.projects.all()),
            'desc': self.desc
        }


class ProjectBuilder(models.Model):
    name = models.CharField(max_length=255)
    cover_image = models.ImageField(upload_to='builders/', null=True, blank=True)
    desc = models.TextField(default='', blank=True)

    class Meta:
        verbose_name = 'Builder'
        verbose_name_plural = 'Builders'

    def __str__(self):
        return self.name

    def list_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': self.name,
            'cover_image': self.cover_image.url if self.cover_image else None,
            'projects': map(lambda item: {'name': item.name, 'id': item.id}, self.projects.all()),
            'desc': self.desc
        }


class ProjectArchitect(models.Model):
    name = models.CharField(max_length=255)
    cover_image = models.ImageField(upload_to='architects/', null=True, blank=True)
    desc = models.TextField(default='', blank=True)

    class Meta:
        verbose_name = 'Architect'
        verbose_name_plural = 'Architects'

    def __str__(self):
        return self.name

    def list_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': self.name,
            'cover_image': self.cover_image.url if self.cover_image else None,
            'projects': map(lambda item: {'name': item.name, 'id': item.id}, self.projects.all()),
            'desc': self.desc
        }


class ProjectAttribute(models.Model):
    icon = models.ImageField(upload_to='projects/attributes/', null=True, blank=True)
    name = models.CharField(max_length=300)
    slug = models.CharField(max_length=255, unique=True)
    category = models.CharField(max_length=300)

    class Meta:
        verbose_name = 'Attribute'
        verbose_name_plural = 'Attributes'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = ModelUtils.transform_field_into_slug(self.name)
        super().save(*args, **kwargs)

    def list_item_serializer(self):
        return {
            'id': self.id,
            'icon': self.icon.url if self.icon else None,
            'name': self.name,
            'slug': urllib.parse.quote(self.slug),
            'category': self.category
        }

    def list_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': self.name,
            'icon': self.icon.url if self.icon else None,
            'category': self.category,
            'projects': map(lambda item: {'name': item.name, 'id': item.id}, self.projects.all()),
        }

    @staticmethod
    def filters_admin():
        return {
            'category': ProjectAttribute.objects.values_list('category', flat=True).distinct()
        }

    @staticmethod
    def form_options_admin():
        return {
            'categories': ProjectAttribute.objects.values_list('category', flat=True).distinct()
        }


class ProjectSuburb(models.Model):
    name = models.CharField(max_length=255)
    council_area = models.CharField(max_length=255, null=True, blank=True)
    postcode = models.CharField(max_length=10, null=True, blank=True)
    region = models.CharField(max_length=100, null=True, blank=True)
    desc = models.TextField(default='', blank=True, null=True)

    class Meta:
        verbose_name = 'Suburb'
        verbose_name_plural = 'Suburbs'

    def __str__(self):
        return self.name + ' - ' + (self.postcode or '')

    def list_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': self.name,
            'postcode': self.postcode,
            'region': self.region,
            'council_area': self.council_area,
            'projects': map(lambda item: {'name': item.name, 'id': item.id}, self.projects.all()),
        }

    def form_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': self.name,
            'postcode': self.postcode,
            'region': self.region,
            'council_area': self.council_area,
            'images': map(lambda item: {'uid': item.id, 'name': item.file.name, 'url': item.file.url},
                          self.assets.filter(type=1)),
        }

    @staticmethod
    def filters_admin():
        return {
            'region': ProjectSuburb.objects.values_list('region', flat=True).distinct(),
            'council_area': ProjectSuburb.objects.values_list('council_area', flat=True).distinct()
        }


class ProjectSuburbAsset(AssetModelClass):
    suburb = models.ForeignKey(
        ProjectSuburb,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='assets'
    )
    file = models.FileField(upload_to='suburbs/')
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='uploaded_suburb_files'
    )


class Project(models.Model):
    sale_status_enum = (
        (0, 'Unknown'),
        (1, 'Sold Out'),
        (2, 'On Sale'),
        (3, 'Off Market'),
    )

    promotion_status_enum = (
        (0, 'Unknown'),
        (1, 'On Promotion'),
        (2, 'Not On Promotion'),
    )

    completion_status_enum = (
        (0, 'Unknown'),
        (1, 'Not Started'),
        (2, 'In Progress'),
        (3, 'Completed'),
        (4, 'Canceled'),
    )

    priority_level_enum = (
        (0, 'Default'),
        (1, 'lowest'),
        (2, 'Medium'),
        (3, 'Highest')
    )

    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=255, unique=True)
    developers = models.ManyToManyField(
        ProjectDeveloper,
        blank=True,
        related_name='projects'
    )
    builders = models.ManyToManyField(
        ProjectBuilder,
        blank=True,
        related_name='projects'
    )
    architects = models.ManyToManyField(
        ProjectArchitect,
        blank=True,
        related_name='projects'
    )
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(auto_now_add=True)
    highlights = models.TextField(default='')
    description = models.TextField(default='')
    publish_status = models.IntegerField(choices=ModelUtils.publish_status_enum, default=1)
    sale_status = models.IntegerField(choices=sale_status_enum, default=1)
    promotion_status = models.IntegerField(choices=promotion_status_enum, default=1)
    promotion_date = models.DateTimeField(null=True, blank=True)
    completion_status = models.IntegerField(choices=completion_status_enum, default=1)
    completion_date = models.DateTimeField()
    completion_note = models.TextField(null=True, blank=True)
    priority_level = models.IntegerField(choices=priority_level_enum, default=0)
    address = models.CharField(max_length=255)
    suburb = models.ForeignKey(
        ProjectSuburb,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='projects'
    )
    coordinates = models.JSONField(null=True, blank=True)
    dropbox_link = models.CharField(max_length=1000, null=True, blank=True)
    cover_image = models.ImageField(upload_to='projects/cover/', null=True, blank=True)
    attributes = models.ManyToManyField(
        ProjectAttribute,
        blank=True,
        related_name='projects'
    )
    visible_to_non_login = models.BooleanField(default=True)
    views = models.IntegerField(default=0)
    unauthenticated_views = models.IntegerField(default=0)
    hide_price = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.slug = ModelUtils.transform_field_into_slug(self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    def list_item_serializer(self, request=None, map_list=False):
        if not map_list:
            output = {
                'name': self.name,
                'slug': urllib.parse.quote(self.slug),
                'image': self.cover_image.url,
                'updated': self.updated,
                'highlights': self.highlights,
                'address': self.address,
                'suburb': self.suburb.name,
                'hide_price': self.hide_price,
                'suites': [],
            }

            for item in self.suites.values_list('suite_type', flat=True).distinct():
                suites = {
                    'type': item,
                    'items': []
                }
                for suite in self.suites \
                        .filter(suite_type=item) \
                        .order_by('bedroom', 'bathroom', 'car_space', 'min_price', 'max_price'):
                    suites['items'].append(suite.item_serializer())
                output['suites'].append(suites)
        else:
            visible = self.visible_to_non_login
            if request:
                visible = request.user.id or self.visible_to_non_login
            output = {
                'id': self.id,
                'slug': urllib.parse.quote(self.slug),
                'name': self.name,
                'coordinates': self.coordinates,
                'visible': visible
            }

        return output

    def detail_item_serializer(self, map_view=False):
        output = self.list_item_serializer(map_list=False)
        if not map_view:
            output['description'] = self.description
            output['images'] = {
                'name': self.name,
                'items': map(lambda item: item.list_item_serializer(), self.project_assets.filter(type=1))
            }
            output['files'] = map(lambda item: item.list_item_serializer(), self.project_assets.filter(type=2))
            output['estimated_completion'] = self.completion_date
            output['location'] = self.coordinates
            output['developer'] = ', '.join(map(lambda item: item.name, self.developers.all()))
            output['builder'] = ', '.join(map(lambda item: item.name, self.builders.all()))
            output['architect'] = ', '.join(map(lambda item: item.name, self.architects.all()))
            output['product_mix'] = ', '.join(
                map(lambda item: (str(item) if item > int(item) else str(int(item))) if int(item) != 0 else 'Studio',
                    self.suites.values_list('bedroom', flat=True).distinct())) \
                                    + ' bedrooms'
            output['promotion'] = self.promotion_status
            output['attributesList'] = map(lambda item: item.list_item_serializer(), self.attributes.all())

        return output

    def list_item_serializer_admin(self):
        output = {
            'id': self.id,
            'key': self.id,
            'name': self.name,
            'slug': urllib.parse.quote(self.slug),
            'updated': self.updated,
            'address': self.address,
            'suburb': {
                'name': self.suburb.name,
                'id': self.suburb.id
            } if self.suburb else None,
            'suites': [],
            'developers': map(lambda developer: {'name': developer.name, 'id': developer.id}, self.developers.all()),
            'visible_to_non_login': self.visible_to_non_login,
            'sale_status': {
                'text': ModelUtils.get_string_value_from_enum(
                    self.sale_status_enum, self.sale_status
                ) if self.sale_status else None,
                'color': ModelUtils.map_enum_value(
                    self.sale_status,
                    ['#f50', '#87d068', '#2db7f5', '#f50']
                ) if self.sale_status else None
            },
            'completion_status': {
                'text': ModelUtils.get_string_value_from_enum(
                    self.completion_status_enum, self.completion_status
                ) if self.completion_status else None,
                'color': ModelUtils.map_enum_value(
                    self.completion_status,
                    ['warning', 'default', 'processing', 'success', 'warning']
                ) if self.completion_status else None
            },
            'completion_date': self.completion_date,
            'publish_status': {
                'text': ModelUtils.get_string_value_from_enum(
                    ModelUtils.publish_status_enum, self.publish_status
                ) if self.publish_status else None,
                'color': ModelUtils.map_enum_value(
                    self.publish_status,
                    ['#f50', '#87d068', '#2db7f5', '#f50']
                ) if self.publish_status else None
            },
            'promotion_status': ModelUtils.map_enum_value(
                self.promotion_status,
                ['Unknown', 'Yes', 'No']
            ) if self.promotion_status else None,
            'unauthenticated_views': self.unauthenticated_views,
            'authenticated_views': self.views,
            'enquires_count': self.enquiries.count(),
            'priority_level': ModelUtils.get_string_value_from_enum(self.priority_level_enum, self.priority_level)
        }
        for item in self.suites.values_list('suite_type', flat=True).distinct():
            suites = {
                'type': item,
                'items': []
            }
            for suite in self.suites.filter(suite_type=item):
                suites['items'].append(suite.item_serializer())
            output['suites'].append(suites)

        return output

    def form_item_serializer_admin(self):
        return {
            'id': self.id,
            'name': self.name,
            'developers': map(lambda item: item.id, self.developers.all()),
            'builders': map(lambda item: item.id, self.builders.all()),
            'architects': map(lambda item: item.id, self.architects.all()),
            'priority_level': self.priority_level,
            'visible_to_non_login': self.visible_to_non_login,
            'hide_price': self.hide_price,
            'publish_status': self.publish_status,
            'sale_status': self.sale_status,
            'completion_status': self.completion_status,
            'completion_date': self.completion_date,
            'promotion_status': self.promotion_status,
            'promotion_date': self.promotion_date,
            'address': self.address,
            'cover_image': self.cover_image.url if self.cover_image else None,
            'coordinates': self.coordinates,
            'suburb': self.suburb.name if self.suburb else None,
            'description': self.description,
            'highlights': self.highlights,
            'suites': map(
                lambda item: item.item_serializer(admin=True),
                self.suites.all().order_by('bedroom', 'bathroom', 'car_space', 'min_price', 'max_price')
            ),
            'attributes': map(lambda item: item.id, self.attributes.all()),
            'dropbox_link': self.dropbox_link,
            'project_images': map(lambda item: {'uid': item.id, 'name': item.file.name, 'url': item.file.url},
                                  self.project_assets.filter(type=1)),
            'project_files': map(lambda item: {'uid': item.id, 'name': item.file.name, 'url': item.file.url},
                                 self.project_assets.filter(type=2)),
        }

    @staticmethod
    def filters_admin():
        return {
            'publish_status': list(ModelUtils.publish_status_enum)[1:],
            'completion_status': list(Project.completion_status_enum)[1:],
            'priority_level': list(Project.priority_level_enum)[1:],
            'promotion_status': ModelUtils.transform_enum(Project.promotion_status_enum, ['Unknown', 'Yes', 'No'])[1:],
            'sale_status': list(Project.sale_status_enum)[1:],
        }

    @staticmethod
    def form_options_admin():
        return {
            'publish_status': list(ModelUtils.publish_status_enum)[1:],
            'priority_level': list(Project.priority_level_enum),
            'developers': map(lambda item: (item.id, item.name), ProjectDeveloper.objects.all()),
            'builders': map(lambda item: (item.id, item.name), ProjectBuilder.objects.all()),
            'architects': map(lambda item: (item.id, item.name), ProjectArchitect.objects.all()),
            'completion_status': list(Project.completion_status_enum)[1:],
            'promotion_status': list(Project.promotion_status_enum)[1:],
            'sale_status': list(Project.sale_status_enum)[1:],
            'suite_type': list(ProjectSuite.suite_type_enum)[1:],
            'attributes': {
                'categories': ProjectAttribute.objects.all().values_list('category', flat=True).distinct(),
                'items': map(lambda item: item.list_item_serializer(), ProjectAttribute.objects.all())
            },
        }

    def integration_item_serializer(self):
        return {
            'id': self.id,
            'name': self.name,
            'address': self.address,
            'publish_status': {
                'text': ModelUtils.get_string_value_from_enum(ModelUtils.publish_status_enum, self.publish_status),
                'value': self.publish_status
            },
            'sales_status': {
                'text': ModelUtils.get_string_value_from_enum(self.sale_status_enum, self.sale_status),
                'value': self.sale_status
            },
            'priority_level': {
                'text': ModelUtils.get_string_value_from_enum(self.priority_level_enum, self.priority_level),
                'value': self.priority_level
            },
            'completion_status': {
                'text': ModelUtils.get_string_value_from_enum(self.completion_status_enum, self.completion_status),
                'value': self.completion_status
            },
            'completion_date': self.completion_date,
            'promotion_status': {
                'text': ModelUtils.get_string_value_from_enum(self.promotion_status_enum, self.promotion_status),
                'value': self.promotion_status
            },
            'promotion_date': self.promotion_date,
            'developer': map(lambda item: item.list_item_serializer_admin(), self.developers.all()),
            'suburb': {
                'name': self.suburb.name,
                'id': self.suburb.id
            } if self.suburb else None,
            'description': self.description,
            'highlights': self.highlights,
            'suites': map(
                lambda item: item.item_serializer(),
                self.suites.all().order_by('bedroom', 'bathroom', 'car_space', 'min_price', 'max_price')
            ),
        }

    def csv_item_serializer(self):
        return {
            'id': self.id,
            'name': self.name,
            'address': self.address,
            'publish_status': ModelUtils.get_string_value_from_enum(ModelUtils.publish_status_enum,
                                                                    self.publish_status),
            'sale_status': ModelUtils.get_string_value_from_enum(self.sale_status_enum, self.sale_status),
            'priority_level': ModelUtils.get_string_value_from_enum(self.priority_level_enum, self.priority_level),
            'completion_status': ModelUtils.get_string_value_from_enum(self.completion_status_enum,
                                                                       self.completion_status),
            'completion_date': self.completion_date,
            'promotion_status': ModelUtils.get_string_value_from_enum(self.promotion_status_enum,
                                                                      self.promotion_status),
            'promotion_date': self.promotion_date,
            'developers': ", ".join(map(lambda item: item.name, self.developers.all())),
            'suburb': self.suburb.name,
            'description': self.description,
            'highlights': self.highlights,
            'suites': "\r\n".join(map(
                lambda item: item.csv_item_serializer(),
                self.suites.all().order_by('bedroom', 'bathroom', 'car_space', 'min_price', 'max_price')
            ))
        }


class ProjectSuite(models.Model):
    suite_type_enum = (
        (0, 'Unknown'),
        (1, 'Apartment'),
        (2, 'Townhouse'),
        (3, 'House'),
        (4, 'Duplex'),
        (5, 'Other')
    )

    bedroom = models.FloatField()
    bathroom = models.FloatField()
    car_space = models.FloatField()
    suite_type = models.IntegerField(choices=suite_type_enum, default=1)
    min_price = models.FloatField()
    max_price = models.FloatField()
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name='suites'
    )

    def __str__(self):
        return 'Suite: {}'.format(ModelUtils.get_string_value_from_enum(self.suite_type_enum, self.suite_type))

    def item_serializer(self, admin=False):
        return {
            'id': self.id,
            'key': self.id,
            'suite_type': self.suite_type,
            'bedroom': self.bedroom,
            'bathroom': self.bathroom,
            'car_space': self.car_space,
            'min': self.min_price if (not self.project.hide_price) or admin else 0,
            'max': self.max_price if (not self.project.hide_price) or admin else 0
        }

    def csv_item_serializer(self):
        return f"bed: {self.bedroom}, bath: {self.bathroom}, car: {self.car_space}, type: {self.suite_type}, min: {self.min_price}, max: {self.max_price}"


def asset_directory(instance, filename):
    return 'projects/{}/{}'.format(instance.project.slug if instance.project else 'unknown', filename)


class ProjectAsset(AssetModelClass):
    project = models.ForeignKey(
        Project,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='project_assets'
    )
    file = models.FileField(upload_to=asset_directory)
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='uploaded_project_files'
    )
