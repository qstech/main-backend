from django.urls import path, include

app_name = 'apps'

urlpatterns = [
    path('users/', include('commonrealty.apps.users.urls', namespace='users')),
    path('articles/', include('commonrealty.apps.articles.urls', namespace='articles')),
    path('projects/', include('commonrealty.apps.projects.urls', namespace='projects')),
    path('pages/', include('commonrealty.apps.pages.urls', namespace='pages')),
    path('forms/', include('commonrealty.apps.forms.urls', namespace='forms')),
    path('team/', include('commonrealty.apps.team.urls', namespace='team'))
]