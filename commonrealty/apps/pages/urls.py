from django.urls import path

from commonrealty.apps.pages.admin_views import get_admin_page_metas_by_scope, upload_page_asset, update_page_asset
from commonrealty.apps.pages.views import get_page_detail, get_page_metas_by_scope

app_name = 'pages'

urlpatterns = [
    path('', get_page_metas_by_scope, name='get_page_metas_by_scope'),
    path('admin/', get_admin_page_metas_by_scope, name='get_admin_page_metas_by_scope'),
    path('admin/file-upload/', upload_page_asset, name='upload_page_asset'),
    path('admin/update/', update_page_asset, name='update_page_asset'),
    path('<str:page_slug>/', get_page_detail, name='get_page_detail')
]