from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.generics import get_object_or_404
from rest_framework.utils import json
from rest_framework_simplejwt import authentication

from commonrealty.apps.pages.models import PageMeta, Page, PageAsset
from commonrealty.apps.users.custom_permission import IsAuthenticatedAndInternal
from commonrealty.apps.utils import HttpUtils, ImageUtils


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_page_metas_by_scope(request):
    scope_list = request.data.get('scopes')
    response = {}

    if type(scope_list) != list or len(scope_list) == 0:
        return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed')

    if type(scope_list) == list:
        for scope in scope_list:
            for meta in PageMeta.objects.filter(scope=scope):
                response[meta.key] = meta.item_serializer_admin()

    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def upload_page_asset(request):
    file_data = request.data.get('file')
    if file_data:
        page_asset = PageAsset()

        file = ImageUtils.covert_base64_file(file_data)
        page_asset.file.save(name=file['name'], content=file['content'])
        page_asset.name = file['name']
        page_asset.created_by_id = request.user.id
        page_asset.save()

        if page_asset.file.url:
            return HttpUtils.FormattedResponse(data=page_asset.file.url, status_code=status.HTTP_201_CREATED)

    return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed')


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def update_page_asset(request):
    page_metas = request.data
    if page_metas:
        for item in page_metas:
            meta = get_object_or_404(PageMeta, key=item)
            meta.value = json.dumps(request.data[item]) or request.data[item]
            meta.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_202_ACCEPTED)
    return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed')




