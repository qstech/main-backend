from django.contrib import admin
from commonrealty.apps.pages.models import PageMeta, Page, PageAsset


class MetaInline(admin.TabularInline):
    model = PageMeta
    can_delete = True
    extra = 0


class AssetInline(admin.TabularInline):
    model = PageAsset
    can_delete = True
    extra = 0


class PageAdmin(admin.ModelAdmin):
    inlines = (MetaInline, AssetInline)


class PageMetaAdmin(admin.ModelAdmin):
    list_display = ('title', 'key', 'value', 'scope')


class PageAssetAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_admin_filename_col', 'get_admin_url_col')


admin.site.register(Page, PageAdmin)
admin.site.register(PageMeta, PageMetaAdmin)
admin.site.register(PageAsset, PageAssetAdmin)
