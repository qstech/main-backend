import json
import urllib.parse

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from commonrealty.apps.utils import ModelUtils


class Page(models.Model):
    title = models.CharField(max_length=300)
    slug = models.SlugField()
    meta_mapping = models.TextField()
    publish_status = models.IntegerField(choices=ModelUtils.publish_status_enum, default=1)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = ModelUtils.transform_field_into_slug(self.title)
        super().save(*args, **kwargs)

    def detail_item_serializer(self):
        output = {
            'title': self.title,
            'slug': urllib.parse.quote(self.slug),
            'meta_values': {}
        }
        required_meta = map(lambda item: item.lstrip(' '), self.meta_mapping.split(','))
        for i in required_meta:
            meta = self.page_metas.filter(key=i, publish_status=1)
            if meta.exists():
                output['meta_values'][i] = json.loads(meta.first().value) or meta.first().value
            else:
                output['meta_values'][i] = None
        return output


class PageMeta(models.Model):
    title = models.CharField(max_length=255)
    key = models.SlugField(unique=True)
    value = models.TextField()
    options = models.JSONField(null=True, blank=True)
    page = models.ForeignKey(
        Page,
        on_delete=models.CASCADE,
        related_name='page_metas',
        null=True,
        blank=True
    )
    scope = models.CharField(max_length=255, null=True, blank=True)
    publish_status = models.IntegerField(choices=ModelUtils.publish_status_enum, default=1)

    def __str__(self):
        return self.key

    def item_serializer(self):
        return {
            'key': self.key,
            'value': json.loads(self.value) or self.value
        }

    def item_serializer_admin(self):
        return {
            'id': self.id,
            'title': self.title,
            'key': self.key,
            'value': json.loads(self.value) or self.value,
            'scope': self.scope,
            'options': self.options
        }

    @staticmethod
    def options_admin(scope):
        if scope == 'home':
            return {
                'content_type': ['Video', 'Image', 'Youtube Link']
            }
        return


def asset_directory(instance, filename):
    return 'pages/{}/{}'.format(instance.page.slug if instance.page else 'unknown', filename)


class PageAsset(models.Model):
    page = models.ForeignKey(
        Page,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='page_assets'
    )
    name = models.CharField(max_length=300, null=True, blank=True)
    file = models.FileField(upload_to=asset_directory)
    created = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='uploaded_page_files'
    )

    def __str__(self):
        return self.file.url

    def get_admin_filename_col(self):
        return self.file.name
    get_admin_filename_col.short_description = 'Filename'

    def get_admin_url_col(self):
        return self.file.url
    get_admin_url_col.short_description = 'URL'
