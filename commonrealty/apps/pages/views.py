from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from commonrealty.apps.pages.models import Page, PageMeta
from commonrealty.apps.utils import HttpUtils


@api_view(['GET'])
def get_page_detail(request, page_slug):
    page = get_object_or_404(Page, slug=page_slug, publish_status=1)
    return Response(page.detail_item_serializer(), status=status.HTTP_200_OK)


@api_view(['POST'])
def get_page_metas_by_scope(request):
    scope_list = request.data.get('scopes')
    response = {}

    if type(scope_list) != list or len(scope_list) == 0:
        return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed')

    if type(scope_list) == list:
        for scope in scope_list:
            for meta in PageMeta.objects.filter(scope=scope):
                response[meta.key] = meta.item_serializer_admin()

    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)