from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'commonrealty.apps.pages'
