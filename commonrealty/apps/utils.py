import base64
import math
from datetime import timedelta, datetime

from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.db import models
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response


class ImageUtils:
    @staticmethod
    def covert_base64_file(base64_string):
        file_format, img_str = base64_string.split(';base64,')
        if file_format.split('/')[-1] == 'svg+xml':
            ext = 'svg'
        else:
            ext = file_format.split('/')[-1]
        return {
            'name': '{}.{}'.format(datetime.now(), ext),
            'content': ContentFile(base64.b64decode(img_str))
        }


class GeneralUtils:
    @staticmethod
    def change_dict_key(dict, old_key, new_key):
        if old_key in dict:
            dict[new_key] = dict[old_key]
            del dict[old_key]
        return dict

    @staticmethod
    def value_in_dict_and_not_null(dict, value_key, assertion=None, not_assertion=None):
        if value_key in dict and dict.get(value_key):
            if not_assertion:
                return dict.get(value_key) != not_assertion and (dict.get(value_key) == assertion if assertion else True)
            return True
        return False

class ModelUtils:
    publish_status_enum = (
        (0, 'Unknown'),
        (1, 'Active'),
        (2, 'Draft'),
        (3, 'Deleted')
    )

    @staticmethod
    def transform_field_into_slug(field):
        return field.replace(' ', '-').lower()

    @staticmethod
    def compare_date_delta_in_month(date_string1, date_string2):
        second_delta = date_string1.timestamp() - date_string2.timestamp()
        return math.ceil(second_delta / 60 / 60 / 24 / 28)

    @staticmethod
    def get_future_datetime_from_delta(month_delta):
        now = timezone.now()
        return now + timedelta(days=month_delta*28)

    @staticmethod
    def get_string_value_from_enum(enum, num_value):
        return enum[num_value][1]

    @staticmethod
    def map_enum_value(value, map_value_list):
        return map_value_list[value] or 'unknown'

    @staticmethod
    def transform_enum(enum, target_list):
        for index, value in enumerate(target_list):
            target_list[index] = (index, value)
        return target_list


class AssetModelClass(models.Model):
    file_type_enum = (
        (0, 'Unknown'),
        (1, 'Image'),
        (2, 'File')
    )

    name = models.CharField(max_length=300, null=True, blank=True)
    file = models.FileField(upload_to='files/')
    type = models.IntegerField(choices=file_type_enum, default=0)
    created = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    class Meta:
        abstract = True

    def __str__(self):
        return '{}'.format(ModelUtils.get_string_value_from_enum(self.file_type_enum, self.type))

    def list_item_serializer(self):
        if self.type == 1:
            return self.file.url
        else:
            return {
                'link': self.file.url,
                'name': self.file.name
            }


class HttpUtils:
    @staticmethod
    def FormattedResponse(
            status_code=status.HTTP_200_OK,
            status_text='success', error_code='',
            error_message='',
            data=None,
            *args, **kwargs
    ):
        output_payload = {
            'status': status_text,
            'err_code': error_code,
            'err_message': error_message,
            'data': data
        }
        return Response(output_payload, status=status_code, *args, **kwargs)
