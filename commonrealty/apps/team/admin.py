from django.contrib import admin
from commonrealty.apps.team import models

admin.site.register(models.TeamMember)