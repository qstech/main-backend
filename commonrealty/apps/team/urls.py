from django.urls import path

from commonrealty.apps.team import views, admin_views

app_name = 'team'

urlpatterns = [
    path('members/', views.get_team_member_list, name='get_team_member_list'),
    path('admin/members/list/', admin_views.get_admin_team_members_list, name='get_admin_team_members_list'),
    path('admin/members/edit-form-data/', admin_views.get_admin_team_member_details,
         name='get_admin_team_member_details'),
    path('admin/members/asset/', admin_views.upload_team_asset, name='upload_team_asset'),
    path('admin/members/post/', admin_views.post_team_member_form, name='post_team_member_form')
]