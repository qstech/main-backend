from django.db import models
from django.utils import timezone

from commonrealty.apps.utils import GeneralUtils


class TeamMember(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    title = models.CharField(max_length=255, null=True, blank=True)
    desc = models.TextField(null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    asset_list = models.CharField(null=True, blank=True, max_length=500)
    strength = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(auto_now=True)
    display_order = models.IntegerField(default=1)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    class Meta:
        ordering = ('display_order', '-id')

    def list_item_serializer(self):
        assets = []
        avatar = ''
        if self.asset_list:
            for i in self.asset_list.split(','):
                if i:
                    image = TeamMemberAssets.objects.get(id=i)
                    assets.append({
                        'id': image.id,
                        'file': image.file.url
                    })
        if len(assets) > 0:
            avatar = assets[0]['file']
        return {
            'id': self.id,
            'avatar': avatar,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'title': self.title,
            'desc': self.desc,
            'phone': self.phone,
            'email': self.email,
            'strength': self.strength,
            'is_active': 'Active' if self.is_active else 'Inactive',
            'assets': assets,
            'display_order': self.display_order
        }

    @staticmethod
    def filters_admin():
        strength_options = []
        team_members = TeamMember.objects.all()
        for i in team_members:
            if i.strength:
                for option in i.strength.split(','):
                    if option not in strength_options:
                        strength_options.append(option)
        return {
            'is_active': (
                (True, 'Active'),
                (False, 'Inactive')
            ),
            'strength': strength_options
        }


class TeamMemberAssets(models.Model):
    created = models.DateTimeField(default=timezone.now)
    file = models.ImageField(upload_to='team/')
