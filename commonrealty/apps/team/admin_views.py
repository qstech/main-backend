from django.core.paginator import Paginator
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.generics import get_object_or_404
from rest_framework_simplejwt import authentication

from commonrealty.apps.team import models
from commonrealty.apps.users.custom_permission import IsAuthenticatedAndInternal
from commonrealty.apps.utils import HttpUtils, ImageUtils


@api_view(['POST'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_team_members_list(request):
    per_page = request.data.get('pageSize') or 10
    page = request.data.get('current') or 1
    filters = request.data.get('filter') or None
    sorter = request.data.get('sorter') or 'id'
    sorter_direction = request.data.get('sorterDirection') or '-'

    members = models.TeamMember.objects.all()

    if filters:
        filter_options = {}
        for i in filters:
            value = map(lambda item: int(item), filters[i]) if filters[i] else None
            if value:
                if i == 'is_active':
                    filter_options['is_active__in'] = value
        members = members.filter(**filter_options)

    members = members.order_by('{}{}'.format(sorter_direction, sorter))

    paginator = Paginator(members, per_page)

    response = {
        'total': members.count(),
        'current': page,
        'per_page': per_page or 10,
        'total_pages': paginator.num_pages,
        'items': map(lambda item: item.list_item_serializer(), paginator.get_page(page)),
        'filters': models.TeamMember.filters_admin()
    }

    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def get_admin_team_member_details(request):
    team_member_id = request.GET.get('team_member_id')

    response = {
        'options': models.TeamMember.filters_admin(),
        'data': None
    }

    if team_member_id and team_member_id != 'new':
        team_member = models.TeamMember.objects.filter(id=team_member_id)
        response['data'] = team_member.first().list_item_serializer() if team_member.exists() else None

    return HttpUtils.FormattedResponse(data=response, status_code=status.HTTP_200_OK)


@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def post_team_member_form(request):
    if request.method == 'PUT':
        instance = get_object_or_404(models.TeamMember, id=request.data.get('id'))
        if 'first_name' in request.data:
            instance.first_name = request.data.get('first_name')
        if 'last_name' in request.data:
            instance.last_name = request.data.get('last_name')
        if 'title' in request.data:
            instance.title = request.data.get('title')
        if 'email' in request.data:
            instance.email = request.data.get('email')
        if 'phone' in request.data:
            instance.phone = request.data.get('phone')
        if 'desc' in request.data:
            instance.desc = request.data.get('desc')
        if 'strength' in request.data:
            instance.strength = request.data.get('strength')
        if 'is_active' in request.data:
            instance.is_active = request.data.get('is_active')
        if 'assets' in request.data:
            instance.asset_list = request.data.get('assets')
        if 'display_order' in request.data:
            instance.display_order = request.data.get('display_order')
        instance.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_202_ACCEPTED)
    if request.method == 'POST':
        if not request.data.get('email') or not request.data.get('first_name') or not request.data.get('last_name'):
            return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed')

        team_member_instance = models.TeamMember(
            email=request.data.get('email'),
            first_name=request.data.get('first_name'),
            last_name=request.data.get('last_name'),
            title=request.data.get('title'),
            phone=request.data.get('phone'),
            desc=request.data.get('desc'),
            strength=request.data.get('strength'),
            is_active=request.data.get('is_active') or False,
            asset_list=request.data.get('assets'),
            display_order=request.data.get('display_order')
        )
        team_member_instance.save()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_201_CREATED)
    if request.method == 'DELETE':
        instance = get_object_or_404(models.TeamMember, id=request.data.get('id'))
        instance.delete()
        return HttpUtils.FormattedResponse(status_code=status.HTTP_204_NO_CONTENT)

    return HttpUtils.FormattedResponse(status_code=status.HTTP_400_BAD_REQUEST, status_text='failed', error_code='400')


@api_view(['POST', 'DELETE'])
@permission_classes([IsAuthenticatedAndInternal])
@authentication_classes([authentication.JWTAuthentication])
def upload_team_asset(request):
    file = request.data.get('file')

    asset = models.TeamMemberAssets()

    file_data = ImageUtils.covert_base64_file(file)
    asset.file.save(name=file_data['name'], content=file_data['content'])

    asset.save()
    return HttpUtils.FormattedResponse(data={
        'id': asset.id,
        'file': asset.file.url
    })
