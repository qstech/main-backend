from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view

from commonrealty.apps.team import models
from commonrealty.apps.utils import HttpUtils


@api_view(['GET'])
def get_team_member_list(request):
    team_members = models.TeamMember.objects.filter(is_active=True)
    return HttpUtils.FormattedResponse(data=map(lambda i: i.list_item_serializer(), team_members))

